package codes.probst.docsearch.version.url;

import java.util.HashMap;
import java.util.Map;

import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.model.VersionModel;

import codes.probst.framework.url.AbstractUrlResolver;

public class DefaultVersionUrlResolver extends AbstractUrlResolver<VersionModel> {

	@Override
	protected Map<String, String> getPathParameters(VersionModel value) {
		Map<String, String> parameters = new HashMap<>();
		
		parameters.put("version-id", value.getId().toString());
		parameters.put("version-code", value.getCode());
		
		ProjectModel project = value.getProject();
		if(project != null) {
			parameters.put("project-id", project.getId().toString());
			parameters.put("project-group-id", project.getGroupId());
			parameters.put("project-artifact-id", project.getArtifactId());
		}
		
		return parameters;
	}
	
	@Override
	protected String getPattern() {
		return "/projects/{project-artifact-id}/{project-id}/{version-code}/{version-id}";
	}
}
