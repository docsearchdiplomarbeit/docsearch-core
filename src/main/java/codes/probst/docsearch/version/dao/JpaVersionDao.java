package codes.probst.docsearch.version.dao;

import java.util.Collection;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.model.ClassModel_;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.packages.model.PackageModel_;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.project.model.ProjectModel_;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.docsearch.version.model.VersionModel_;
import codes.probst.framework.dao.AbstractJpaDao;
import codes.probst.framework.pagination.Pageable;

public class JpaVersionDao extends AbstractJpaDao<VersionModel, Long> implements VersionDao {

	public JpaVersionDao() {
		super(VersionModel.class, VersionModel_.id);
	}

	@Override
	public Collection<VersionModel> findByProjectId(Long projectId) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<VersionModel> criteria = builder.createQuery(VersionModel.class);
		
		criteria.where(
			builder.equal(criteria.from(VersionModel.class).get(VersionModel_.project), projectId)
		);
		return select(criteria, null);
	}
	
	@Override
	public Optional<VersionModel> findByPackageId(Long packageId) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<VersionModel> criteria = builder.createQuery(VersionModel.class);
		Root<VersionModel> root = criteria.from(VersionModel.class);
		Join<VersionModel, PackageModel> packageJoin = root.join(VersionModel_.packages);
		
		criteria.where(
			builder.equal(packageJoin.get(PackageModel_.id), packageId)
		);
		return selectOnce(criteria);
	}
	
	@Override
	public Optional<VersionModel> findByClassId(Long classId) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<VersionModel> criteria = builder.createQuery(VersionModel.class);
		Root<VersionModel> root = criteria.from(VersionModel.class);
		Join<VersionModel, PackageModel> packageJoin = root.join(VersionModel_.packages);
		Join<PackageModel, ClassModel> classJoin = packageJoin.join(PackageModel_.classes);
		
		criteria.where(
			builder.equal(classJoin.get(ClassModel_.id), classId)
		);
		return selectOnce(criteria);
	}

	@Override
	public Optional<VersionModel> findByFullQualifier(String groupId, String artifactId, String versionCode) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<VersionModel> criteria = builder.createQuery(VersionModel.class);
		Root<VersionModel> versionRoot = criteria.from(VersionModel.class);
		criteria.select(versionRoot);
		
		Join<VersionModel, ProjectModel> join = versionRoot.join(VersionModel_.project);
		
		criteria.where(
			builder.and(
				builder.equal(join.get(ProjectModel_.groupId), groupId),
				builder.equal(join.get(ProjectModel_.artifactId), artifactId),
				builder.equal(versionRoot.get(VersionModel_.code), versionCode)
			)
		);
		return selectOnce(criteria);
	}
	
	@Override
	public Collection<VersionModel> findAllUnexported(Pageable pageable) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<VersionModel> criteria = builder.createQuery(VersionModel.class);
		Root<VersionModel> root = criteria.from(VersionModel.class);
		
		criteria.where(
			builder.isFalse(root.get(VersionModel_.exported))
		);
		return select(criteria, pageable);
	}
	
}
