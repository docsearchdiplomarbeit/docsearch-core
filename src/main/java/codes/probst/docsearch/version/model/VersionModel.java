package codes.probst.docsearch.version.model;

import java.time.Instant;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.model.ProjectModel;

/**
 * Representation of a single version of a project.
 * @author Benjamin Probst
 *
 */
@Entity
@Table(name = "VERSION")
@SequenceGenerator(initialValue = 0, name = "VersionSequence", sequenceName="VERSION_SEQ")
public class VersionModel {
	@Id
	@GeneratedValue(generator = "VersionSequence", strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	@Column(name = "code", nullable = false, length = 50)
	private String code;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "version")
	private List<PackageModel> packages;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "projectId", nullable = false)
	private ProjectModel project;
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "REQUIRED_VERSION", joinColumns = {
			@JoinColumn(name = "versionId", nullable = false, updatable = false) 
		}, inverseJoinColumns = {
			@JoinColumn(name = "requiredVersionId", nullable = false, updatable = false)
		}
	)
	private List<VersionModel> requires;
	@Column(name = "importetAt")
	private Instant importedAt;
	@Column(name = "exported", nullable = false)
	private boolean exported;

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<PackageModel> getPackages() {
		return packages;
	}

	public void setPackages(List<PackageModel> packages) {
		this.packages = packages;
	}

	public ProjectModel getProject() {
		return project;
	}

	public void setProject(ProjectModel project) {
		this.project = project;
	}

	public List<VersionModel> getRequires() {
		return requires;
	}

	public void setRequires(List<VersionModel> requires) {
		this.requires = requires;
	}

	public void setImportedAt(Instant importedAt) {
		this.importedAt = importedAt;
	}
	
	public Instant getImportedAt() {
		return importedAt;
	}
	
	public void setExported(boolean exported) {
		this.exported = exported;
	}

	public boolean isExported() {
		return exported;
	}
}
