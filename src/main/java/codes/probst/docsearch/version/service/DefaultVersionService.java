package codes.probst.docsearch.version.service;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.version.dao.VersionDao;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.service.AbstractService;

public class DefaultVersionService extends AbstractService<VersionDao, VersionModel, Long> implements VersionService {
	
	@Override
	public Collection<VersionModel> getByProjectId(Long projectId) {
		return getDao().findByProjectId(projectId);
	}

	@Override
	public Optional<VersionModel> getByPackageId(Long packageId) {
		return getDao().findByPackageId(packageId);
	}
	
	@Override
	public Optional<VersionModel> getByClassId(Long classId) {
		return getDao().findByClassId(classId);
	}
	
	@Override
	public Optional<VersionModel> getByFullQualifier(String groupId, String artifactId, String versionCode) {
		return getDao().findByFullQualifier(groupId, artifactId, versionCode);
	}
	
	@Override
	public Collection<VersionModel> getAllUnexported(Pageable pageable) {
		return getDao().findAllUnexported(pageable);
	}

	
	@Override
	protected boolean isNew(VersionModel object) {
		return object.getId() == null;
	}

}
