package codes.probst.docsearch.version.facade.populator;

import java.util.Optional;

import codes.probst.docsearch.project.facade.ProjectData;
import codes.probst.docsearch.project.facade.ProjectFacade;
import codes.probst.docsearch.project.facade.ProjectPopulationOption;
import codes.probst.docsearch.version.facade.VersionData;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.bean.BeanResolver;
import codes.probst.framework.populator.Populator;

public class VersionProjectPopulator implements Populator<VersionModel, VersionData> {
	private BeanResolver beanResolver;
	private ProjectFacade projectFacade;
	
	
	
	public void populate(VersionModel source, VersionData target) {
		Optional<ProjectData> optional = getProjectFacade().getByVersionId(source.getId(), ProjectPopulationOption.OPTION_URL);
		if(optional.isPresent()) {
			target.setProject(optional.get());
		}
	}

	private ProjectFacade getProjectFacade() {
		if(projectFacade == null) {
			projectFacade = beanResolver.resolve(ProjectFacade.class);
		}
		return projectFacade;
	}
	
	public void setBeanResolver(BeanResolver beanResolver) {
		this.beanResolver = beanResolver;
	}
}
