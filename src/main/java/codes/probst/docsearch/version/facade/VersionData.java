package codes.probst.docsearch.version.facade;

import java.util.Collection;

import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.project.facade.ProjectData;

public class VersionData {
	private Long id;
	private String code;
	private String url;
	private ProjectData project;
	private Collection<PackageData> packages;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public ProjectData getProject() {
		return project;
	}
	
	public void setProject(ProjectData project) {
		this.project = project;
	}
	
	public void setPackages(Collection<PackageData> packages) {
		this.packages = packages;
	}
	
	public Collection<PackageData> getPackages() {
		return packages;
	}
}
