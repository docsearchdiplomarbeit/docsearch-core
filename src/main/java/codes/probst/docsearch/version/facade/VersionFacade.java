package codes.probst.docsearch.version.facade;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.facade.Facade;

public interface VersionFacade extends Facade<VersionModel, VersionData, Long> {

	public Optional<VersionData> getByClassId(Long classId, VersionPopulationOption... options);

	public Optional<VersionData> getByPackageId(Long packageId, VersionPopulationOption... options);

	public Collection<VersionData> getByProjectId(Long projectId, VersionPopulationOption... options);

}