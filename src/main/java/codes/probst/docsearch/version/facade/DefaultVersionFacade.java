package codes.probst.docsearch.version.facade;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.docsearch.version.service.VersionService;
import codes.probst.framework.facade.AbstractFacade;

public class DefaultVersionFacade extends AbstractFacade<VersionModel, VersionData, VersionService, Long> implements VersionFacade {
	
	@Override
	public Optional<VersionData> getByClassId(Long classId, VersionPopulationOption... options) {
		Optional<VersionModel> optional = getService().getByClassId(classId);
		
		if(optional.isPresent()) {
			return Optional.of(convertAndPopulate(optional.get(), options));
		}
		
		return Optional.empty();
	}
	
	@Override
	public Optional<VersionData> getByPackageId(Long packageId, VersionPopulationOption... options) {
		Optional<VersionModel> optional = getService().getByPackageId(packageId);
		
		if(optional.isPresent()) {
			return Optional.of(convertAndPopulate(optional.get(), options));
		}
		
		return Optional.empty();
	}
	
	@Override
	public Collection<VersionData> getByProjectId(Long projectId, VersionPopulationOption... options) {
		return convertAndPopulateAll(getService().getByProjectId(projectId), options);
	}
	
}
