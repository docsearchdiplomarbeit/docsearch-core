package codes.probst.docsearch.queue.facade;

public interface QueueEntryFacade {

	public void importEntry(byte[] compiled, byte[] source);

}