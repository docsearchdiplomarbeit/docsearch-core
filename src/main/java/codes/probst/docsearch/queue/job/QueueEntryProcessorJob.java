package codes.probst.docsearch.queue.job;

import java.util.Optional;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codes.probst.docsearch.queue.model.QueueEntryModel;
import codes.probst.docsearch.queue.service.QueueEntryProcessor;
import codes.probst.docsearch.queue.service.QueueEntryService;
import codes.probst.framework.monitoring.StopWatch;

/**
 * Processes one {@code QueueEntryModel} at a time and retrieves the information
 * of the project for the database.
 * 
 * @author Benjamin Probst
 *
 */
@DisallowConcurrentExecution
public class QueueEntryProcessorJob implements Job {
	private static final Logger LOG = LoggerFactory.getLogger(QueueEntryProcessorJob.class);

	private QueueEntryService queueEntryService;
	private QueueEntryProcessor queueEntryProcessor;
	
	/**
	 * Runs a job execution that handles a single {@code QueueEntryModel} if one
	 * is available.
	 */
	@Override
	public void execute(JobExecutionContext ctx) {
		StopWatch watch = new StopWatch();
		watch.start();
		LOG.debug("starting QueueEntryWorker.");

		Optional<QueueEntryModel> optionalEntry = queueEntryService.getNextQueueEntryToProcess();
		if (optionalEntry.isPresent()) {
			QueueEntryModel queueEntry = optionalEntry.get();
			queueEntryProcessor.process(queueEntry);
		} else {
			LOG.debug("no entry to process.");
		}
		LOG.info("ending QueueEntryWorker after {}ms.", watch.stop());
	}

	public void setQueueEntryProcessor(QueueEntryProcessor queueEntryProcessor) {
		this.queueEntryProcessor = queueEntryProcessor;
	}
	
	public void setQueueEntryService(QueueEntryService queueEntryService) {
		this.queueEntryService = queueEntryService;
	}
	
}
