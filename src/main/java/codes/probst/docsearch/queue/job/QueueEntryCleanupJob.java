package codes.probst.docsearch.queue.job;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codes.probst.docsearch.queue.service.QueueEntryService;
import codes.probst.framework.monitoring.StopWatch;

@DisallowConcurrentExecution
public class QueueEntryCleanupJob implements Job {
	private static final Logger LOG = LoggerFactory.getLogger(QueueEntryCleanupJob.class);
	
	private QueueEntryService queueEntryService;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch watch = new StopWatch();
		watch.start();
		try {
			Instant instant = Instant.now().minus(7, ChronoUnit.DAYS);
			queueEntryService.removeBefore(instant);
		} finally {
			LOG.info("queue entry cleanup finished after {}ms.", watch.stop());
		}
	}
	
	public void setQueueEntryService(QueueEntryService queueEntryService) {
		this.queueEntryService = queueEntryService;
	}
	
}
