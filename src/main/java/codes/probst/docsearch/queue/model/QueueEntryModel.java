package codes.probst.docsearch.queue.model;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import codes.probst.docsearch.analyzer.projectinformation.ProjectType;

/**
 * Persistance type for queue entries that are processed at a later time.
 * @author Benjamin Probst
 *
 */
@Entity
@Table(name = "QUEUE_ENTRY")
@SequenceGenerator(initialValue = 0, name = "QueueEntrySequence", sequenceName="QUEUE_ENTRY_SEQ")
public class QueueEntryModel {
	@Id
	@GeneratedValue(generator = "QueueEntrySequence", strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	@Column(name = "position")
	private Long position;
	@Lob
	@Column(name = "compiledData", nullable = false, length = Integer.MAX_VALUE)
	private byte[] compiledData;
	@Lob
	@Column(name = "sourceData", length = Integer.MAX_VALUE)
	private byte[] sourceData;
	@Column(name = "state", nullable = false)
	private QueueEntryState state;
	@Column(name = "groupId", nullable = false, length = 255)
	private String groupId;
	@Column(name = "artifactId", nullable = false, length = 255)
	private String artifactId;
	@Column(name = "version", nullable = false, length = 50)
	private String version;
	@Column(name = "type", nullable = false)
	private ProjectType type;
	@Column(name = "modificationDate")
	private Instant modificationDate;

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Returns the position.
	 * @return The position
	 */
	public Long getPosition() {
		return position;
	}

	/**
	 * Sets the position.
	 * @param position The position
	 */
	public void setPosition(Long position) {
		this.position = position;
	}

	/**
	 * Returns the compiled bytes of a project.
	 * @return The compield bytes
	 */
	public byte[] getCompiledData() {
		return compiledData;
	}

	/**
	 * Sets the compiled bytes of a project.
	 * @param compiledData The compiled bytes
	 */
	public void setCompiledData(byte[] compiledData) {
		this.compiledData = compiledData;
	}

	/**
	 * Returns the source bytes of a project.
	 * @return The source bytes
	 */
	public byte[] getSourceData() {
		return sourceData;
	}

	/**
	 * Sets the source bytes of a project.
	 * @param sourceData The source bytes
	 */
	public void setSourceData(byte[] sourceData) {
		this.sourceData = sourceData;
	}

	/**
	 * Returns the state of the entry.
	 * @return The state
	 */
	public QueueEntryState getState() {
		return state;
	}

	/**
	 * Sets the state of the entry.
	 * @param state The state
	 */
	public void setState(QueueEntryState state) {
		this.state = state;
	}

	/**
	 * Returns the groupId of the project.
	 * @return The group id
	 */
	public String getGroupId() {
		return groupId;
	}
	
	/**
	 * Sets the groupId of the project.
	 * @param groupId The groupId
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * Returns the artifactId of the project.
	 * @return The artifactId
	 */
	public String getArtifactId() {
		return artifactId;
	}

	/**
	 * Sets the artifactId of the project.
	 * @param artifactId The artifactId
	 */
	public void setArtifactId(String artifactId) {
		this.artifactId = artifactId;
	}

	/**
	 * Returns the version of the project.
	 * @return The version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the version of the project.
	 * @param version The version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Sets the type of the project.
	 * @return The type
	 */
	public ProjectType getType() {
		return type;
	}

	/**
	 * Sets the type of the project.
	 * @param type The type
	 */
	public void setType(ProjectType type) {
		this.type = type;
	}

	/**
	 * Returns the last modification date.
	 * @return The modification date
	 */
	public Instant getModificationDate() {
		return modificationDate;
	}
	
	/**
	 * Sets the last modification date.
	 * @param modificationDate The modification date
	 */
	public void setModificationDate(Instant modificationDate) {
		this.modificationDate = modificationDate;
	}
	
	
}
