package codes.probst.docsearch.queue.model;

/**
 * Defines the processing state of a {@code QueueEntryModel}.
 * @author Benjamin Probst
 *
 */
public enum QueueEntryState {
	NEW,
	SUCCESSFULLY,
	SKIPPED,
	RUNNING,
	ERROR,
	IGNORED;
}
