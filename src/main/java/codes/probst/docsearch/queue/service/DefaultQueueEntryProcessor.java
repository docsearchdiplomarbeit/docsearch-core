package codes.probst.docsearch.queue.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codes.probst.docsearch.analyzer.projectinformation.ProjectType;
import codes.probst.docsearch.analyzer.service.ClassAnalysis;
import codes.probst.docsearch.analyzer.service.ClassAnalyzerService;
import codes.probst.docsearch.analyzer.service.ClassUtil;
import codes.probst.docsearch.analyzer.service.CodeSnippedService;
import codes.probst.docsearch.analyzer.service.MethodAnalysis;
import codes.probst.docsearch.analyzer.service.MethodUsageAnalysis;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.model.ClassUsageModel;
import codes.probst.docsearch.clazz.service.ClassService;
import codes.probst.docsearch.dependency.Dependency;
import codes.probst.docsearch.dependency.service.DependencyService;
import codes.probst.docsearch.doclet.DocSearchDoclet;
import codes.probst.docsearch.doclet.Parser;
import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.docsearch.method.model.MethodUsageModel;
import codes.probst.docsearch.method.service.MethodService;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.packages.service.PackageService;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.project.service.ProjectService;
import codes.probst.docsearch.queue.job.ClassImportPostProcessor;
import codes.probst.docsearch.queue.model.QueueEntryModel;
import codes.probst.docsearch.queue.model.QueueEntryState;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.docsearch.version.service.VersionService;
import codes.probst.framework.file.service.FileService;
import codes.probst.framework.monitoring.StopWatch;

public class DefaultQueueEntryProcessor implements QueueEntryProcessor {
	private static final Logger LOG = LoggerFactory.getLogger(DefaultQueueEntryProcessor.class);
	
	private QueueEntryService queueEntryService;
	private ProjectService projectService;
	private VersionService versionService;
	private PackageService packageService;
	private ClassService classService;
	private MethodService methodService;
	private FileService fileService;
	private ClassAnalyzerService classAnalyzerService;
	private DependencyService dependencyService;
	private CodeSnippedService codeSnippedService;
	private Collection<ClassImportPostProcessor> classImportPostProcessors;

	@Override
	public void process(QueueEntryModel queueEntry) {
		String randomName = UUID.randomUUID().toString();
		Path folder = Paths.get(System.getProperty("java.io.tmpdir")).resolve(randomName);
		try {
			if (handleQueueEntry(queueEntry, folder)) {
			} else {
				queueEntry.setState(QueueEntryState.ERROR);
				queueEntryService.save(queueEntry);
			}
		} catch (Exception e) {
			LOG.error("could not execute next query entry", e);
			
			queueEntry.setState(QueueEntryState.ERROR);
			queueEntryService.save(queueEntry);
			
		} finally {
			try {
				//call gc to prevent windows bug see http://bugs.java.com/bugdatabase/view_bug.do?bug_id=4715154
				System.gc();
				FileUtils.deleteDirectory(folder.toFile());
			} catch (IOException e) {
				LOG.warn("could not delete folder {}.", folder);
			}
		}
	}
	
	/**
	 * Handles a job execution that handles a single {@code QueueEntryModel}.
	 * @return {@code true} if the entry handled successfully and {@code false} otherwise
	 */
	private boolean handleQueueEntry(QueueEntryModel queueEntry, Path folder) {
		queueEntry.setState(QueueEntryState.RUNNING);
		queueEntryService.save(queueEntry);
		
		if(isValidData(queueEntry)) {
			String groupId = queueEntry.getGroupId();
			String artifactId = queueEntry.getArtifactId();
			String versionCode = queueEntry.getVersion();
			
			ProjectModel project = getOrCreateProject(groupId, artifactId);
			
			
			Optional<VersionModel> optionalVersion = versionService.getByFullQualifier(groupId, artifactId, versionCode);
			VersionModel version;
			if(optionalVersion.isPresent()) {
				LOG.info("version {} in project with groupId = {} and artifactId = {} already exists.", optionalVersion.get().getCode(), project.getGroupId(), project.getArtifactId());
				return false;
			} else {
				version = new VersionModel();
				version.setProject(project);
				version.setCode(versionCode);
				
				LOG.info("created version with id: {}, groupId: {}, artifactId: {}, version: {}.", version.getId(), project.getGroupId(), project.getArtifactId(), version.getCode());
			}
			
			Boolean dependencyResult = handleDependencies(queueEntry, project, version);
			if(dependencyResult != null) {
				return dependencyResult.booleanValue();
			}
			
			projectService.save(project);
			versionService.save(version);
			
			Map<String, ClassModel> classesCache = new HashMap<>();
			Map<String, PackageModel> packagesCache = new HashMap<>();
			Map<Long, Collection<MethodModel>> methodsCache = new HashMap<>();
			
			extractSources(folder, queueEntry);
			
			List<ClassAnalysis> classAnalysises = importClasses(queueEntry, folder, version, packagesCache, classesCache);
			if(classAnalysises == null) {
				return false;
			}
			
				
			Boolean methodImportResult = importMethods(version, classAnalysises, classesCache, methodsCache);
			if(methodImportResult != null) {
				return methodImportResult.booleanValue();
			}
			
			Boolean docImportResult = importDocAndSource(queueEntry, folder, version, classAnalysises, packagesCache, classesCache, methodsCache);
		    if(docImportResult != null) {
		    	return docImportResult.booleanValue();
		    }
		    
		    version.setImportedAt(Instant.now());
		    versionService.save(version);
		} else {
			LOG.warn("could not process entry {} becuase not all data is present.", queueEntry.getId());
			return false;
		}
		
		queueEntry.setState(QueueEntryState.SUCCESSFULLY);
		queueEntryService.save(queueEntry);
		
		return true;
	}
	
	private void extractSources(Path folder, QueueEntryModel queueEntry) {
		try(ByteArrayInputStream bais = new ByteArrayInputStream(queueEntry.getSourceData());
			ZipInputStream zis = new ZipInputStream(bais)) {
		
			{
				StopWatch watch = new StopWatch();
				watch.start();
				fileService.extractZipToFolder(zis, folder);
				LOG.info("extracted files after {}ms.", watch.stop());
			}
		} catch (IOException e) {
			LOG.error("could not extract file for queue entry {}", queueEntry.getId());
		}
	}

	private Boolean importDocAndSource(QueueEntryModel queueEntry, Path folder, VersionModel version, List<ClassAnalysis> classAnalysises, Map<String, PackageModel> packagesCache, Map<String, ClassModel> classesCache, Map<Long, Collection<MethodModel>> methodsCache) {
		StopWatch watch = new StopWatch();
		watch.start();
		List<String> packageRootNames = packagesCache.keySet().stream().map(name -> {
			int index = name.indexOf(".");
			if(index == -1) {
				return name;
			}
			return name.substring(0, index);
		}).distinct().filter(name -> !name.isEmpty()).collect(Collectors.toList());
				
		Parser.setup(packageService, classService, methodService);
		Parser.init(version.getId(), packagesCache, classesCache, methodsCache);
		for(String packageName : packageRootNames) {
			try {
				com.sun.tools.javadoc.Main.execute(new String[] {
					"-doclet",
					DocSearchDoclet.class.getName(),
					"-subpackages",
					packageName,
					"-sourcepath",
					folder.toString()
				});
			} catch(Exception e) {
				LOG.error("could not handle javadoc for package {} in version {}", packageName, version.getId());
			}
			
		}
		Parser.release();
		LOG.info("handled javadoc after {}ms.", watch.stop());

		return null;
	}
	
	
	private List<Long> collectVersionIds(VersionModel version, int recursion) {
		List<Long> versionIds = new ArrayList<>();
		versionIds.add(version.getId());
		if(version.getRequires() != null) {
			for(VersionModel requiredVersion : version.getRequires()) {
				ProjectModel project = requiredVersion.getProject();
				if(recursion == 0 || (!"java".equals(project.getGroupId()) && !"jdk".equals(project.getArtifactId()))) {
					versionIds.addAll(collectVersionIds(requiredVersion, recursion + 1));
				}
			}
		}
		return versionIds;
	}
	
	private Boolean importMethods(VersionModel version, Collection<ClassAnalysis> classAnalysises, Map<String, ClassModel> classesCache, Map<Long, Collection<MethodModel>> methodsCache) {
		Map<String, Optional<MethodModel>> methodsSignatureCache = new HashMap<>();
		
		List<Long> versionIds = collectVersionIds(version, 0);

		StopWatch watch = new StopWatch();
		watch.start();
		int i = 1;
		int counter = 0;
		for(ClassAnalysis classAnalysis : classAnalysises) {
			LOG.debug("handling class analysis {} ({} of {})", classAnalysis.getClassName(), i++, classAnalysises.size());
			
			ClassModel clazz = classesCache.get(classAnalysis.getClassName());
			if(clazz == null) {
				String className = ClassUtil.extractClassName(classAnalysis.getClassName());
				String packageName = ClassUtil.extractPackage(classAnalysis.getClassName());
			
				Optional<ClassModel> optionalClass = classService.getByNameAndPackageInVersions(className, packageName, Arrays.asList(version.getId()));
				if(optionalClass.isPresent()) {
					clazz = optionalClass.get();
					classesCache.put(classAnalysis.getClassName(), clazz);
				} else {
					LOG.error("could not find class {} in version {}.", classAnalysis.getClassName(), version.getId());
					return Boolean.FALSE;
				}
			}
			
			Collection<MethodModel> classMethods = new ArrayList<>(classAnalysis.getMethodAnalysis().size());
			for(MethodAnalysis methodAnalysis : classAnalysis.getMethodAnalysis()) {
				counter++;
				MethodModel method = new MethodModel();
				method.setName(methodAnalysis.getName());
				method.setClazz(clazz);
				method.setSignature(methodAnalysis.getSignature());
				
				String cleanedReturnType = ClassUtil.cleanClassName(methodAnalysis.getReturnType());
				ClassModel returnType = classesCache.get(cleanedReturnType);
				if(returnType == null) {
					String returnTypeClassName = ClassUtil.extractClassName(cleanedReturnType);
					String returnTypePackageName =  ClassUtil.extractPackage(cleanedReturnType);

					Optional<ClassModel> optionalReturnType = classService.getByNameAndPackageInVersions(returnTypeClassName, returnTypePackageName, versionIds);
					if(optionalReturnType.isPresent()) {
						returnType = optionalReturnType.get();
						classesCache.put(cleanedReturnType, returnType);
					} else {
						LOG.error("could not find return type {} in versions {}.", methodAnalysis.getReturnType(), versionIds);
						return Boolean.FALSE;
					}
				}
				ClassUsageModel returnTypeClassUsage = classService.getClassUsage(methodAnalysis.getReturnType(), returnType);
				classService.save(returnTypeClassUsage);
				method.setReturnType(returnTypeClassUsage);
				
				
				List<ClassUsageModel> parameters = new ArrayList<>();
				
				for(String fullQualifiedClassName : methodAnalysis.getParameters()) {
					String cleanedFullQualifiedClassName = ClassUtil.cleanClassName(fullQualifiedClassName);
					ClassModel parameterType = classesCache.get(cleanedFullQualifiedClassName);
					if(parameterType == null) {
						String parameterClassName = ClassUtil.extractClassName(cleanedFullQualifiedClassName);
						String parameterPackageName =  ClassUtil.extractPackage(cleanedFullQualifiedClassName);
					
						Optional<ClassModel> optionalParameterType = classService.getByNameAndPackageInVersions(parameterClassName, parameterPackageName, versionIds);
						if(optionalParameterType.isPresent()) {
							parameterType = optionalParameterType.get();
							classesCache.put(cleanedFullQualifiedClassName, parameterType);
						} else {
							LOG.error("could not find parameter type {} in versions {}.", fullQualifiedClassName, versionIds);
							return Boolean.FALSE;
						}
					}
					ClassUsageModel parameterClassUsage = classService.getClassUsage(fullQualifiedClassName, parameterType);
					classService.save(parameterClassUsage);
					parameters.add(parameterClassUsage);
				}
				
				method.setParameters(parameters);
				
				
				methodService.save(method);
				classMethods.add(method);
				methodsSignatureCache.put(buildMethodsSignatureCacheKey(clazz.getId(), method.getReturnType().getClazz().getId(), methodAnalysis.getSignature()), Optional.of(method));
			}
			
			methodsCache.put(clazz.getId(), classMethods);
		}
		LOG.info("imported {} methods after {}ms.", counter, watch.stop());
		
		watch = new StopWatch();
		watch.start();
		counter = 0;
		
		int totalMethodUsageAnalysises = classAnalysises.stream().mapToInt(analysis -> analysis.getMethodUsageAnalysises().size()).sum();
		
		for(ClassAnalysis classAnalysis : classAnalysises) {
			ClassModel clazz = classesCache.get(classAnalysis.getClassName());
			if(clazz == null) {
				String className = ClassUtil.extractClassName(classAnalysis.getClassName());
				String packageName = ClassUtil.extractPackage(classAnalysis.getClassName());
			
				Optional<ClassModel> optionalClass = classService.getByNameAndPackageInVersions(className, packageName, Arrays.asList(version.getId()));
				if(optionalClass.isPresent()) {
					clazz = optionalClass.get();
					classesCache.put(classAnalysis.getClassName(), clazz);
				} else {
					LOG.error("could not find class {} in version {}.", classAnalysis.getClassName(), version.getId());
					return Boolean.FALSE;
				}
			}
			
			for(MethodUsageAnalysis methodUsageAnalysis : classAnalysis.getMethodUsageAnalysises()) {
				counter++;
				
				MethodUsageModel methodUsage = new MethodUsageModel();
				methodUsage.setCallingClass(clazz);
				
				ClassModel calledClass = classesCache.get(methodUsageAnalysis.getCalledClassName());
				if(calledClass == null) {
					String calledClassName = ClassUtil.extractClassName(methodUsageAnalysis.getCalledClassName());
					String calledPackageName =  ClassUtil.extractPackage(methodUsageAnalysis.getCalledClassName());
	
					Optional<ClassModel> optionalCalledClass = classService.getByNameAndPackageInVersions(calledClassName, calledPackageName, versionIds);
					if(optionalCalledClass.isPresent()) {
						calledClass = optionalCalledClass.get();
						classesCache.put(methodUsageAnalysis.getCalledClassName(), calledClass);
					} else {
						continue;
					}
				}
				
				ClassModel returnTypeClass = classesCache.get(methodUsageAnalysis.getReturnType());
				if(returnTypeClass == null) {
					String returnTypeClassName = ClassUtil.extractClassName(methodUsageAnalysis.getReturnType());
					String returnTypePackageName =  ClassUtil.extractPackage(methodUsageAnalysis.getReturnType());
	
					Optional<ClassModel> optionalReturnTypeClass = classService.getByNameAndPackageInVersions(returnTypeClassName, returnTypePackageName, versionIds);
					if(optionalReturnTypeClass.isPresent()) {
						returnTypeClass = optionalReturnTypeClass.get();
						classesCache.put(methodUsageAnalysis.getReturnType(), calledClass);
					} else {
						continue;
					}
				}
				
				
				Optional<MethodModel> optionalMethod = methodsSignatureCache.get(buildMethodsSignatureCacheKey(calledClass.getId(), returnTypeClass.getId(), methodUsageAnalysis.getSignature()));
				if(optionalMethod == null && !version.getRequires().isEmpty()) {
					ClassModel returnType = classesCache.get(methodUsageAnalysis.getReturnType());
					if(returnType == null) {
						String returnTypeClassName = ClassUtil.extractClassName(methodUsageAnalysis.getReturnType());
						String returnTypePackageName =  ClassUtil.extractPackage(methodUsageAnalysis.getReturnType());
		
						Optional<ClassModel> optionalReturnType = classService.getByNameAndPackageInVersions(returnTypeClassName, returnTypePackageName, versionIds);
						if(optionalReturnType.isPresent()) {
							returnType = optionalReturnType.get();
							classesCache.put(methodUsageAnalysis.getReturnType(), returnType);
						} else {
							continue;
						}
					}
					optionalMethod = methodService.getByClassIdAndNameAndReturnTypeIdAndSignature(calledClass.getId(), methodUsageAnalysis.getCalledMethodName(), returnType.getId(), methodUsageAnalysis.getSignature());
					methodsSignatureCache.put(buildMethodsSignatureCacheKey(calledClass.getId(), returnTypeClass.getId(), methodUsageAnalysis.getSignature()), optionalMethod);
				}
						
				if(optionalMethod != null && optionalMethod.isPresent()) {
					methodUsage.setMethod(optionalMethod.get());
				} else {
					continue;
				}
				
				methodUsage.setLineNumber(methodUsageAnalysis.getLineNumber());
				
				if(clazz.getSource() != null) {
					methodUsage.setSnipped(codeSnippedService.getCodeSnipped(new String(clazz.getSource(), Charset.forName("utf-8")), methodUsage));
				}
				
				methodService.save(methodUsage);
				
				if(counter > 0 && counter % 1000 == 0) { 
					LOG.info("imported {} ({}%) method usages after {}ms", counter, Math.round(100f / totalMethodUsageAnalysises * counter), watch.getElapsedTime());
				}
			}
		}
		LOG.info("imported {} method usages after {}ms.", counter, watch.stop());
		
		return null;
	}

	private Boolean handleDependencies(QueueEntryModel queueEntry, ProjectModel project, VersionModel version) {
		Collection<Dependency> dependencies = null;
		try {
			dependencies = dependencyService.getDependencies(queueEntry.getType(), queueEntry.getCompiledData());
			if(dependencies == null || dependencies.isEmpty()) {
				if(ProjectType.PLAIN_JAVA.equals(queueEntry.getType())) {
					throw new IllegalArgumentException();
				}
			}
		} catch(IllegalArgumentException e) {
			//try pseudo pom if not possible to parse
			String pom = "<project><modelVersion>4.0.0</modelVersion><groupId>" + queueEntry.getGroupId() + "</groupId><artifactId>" + queueEntry.getArtifactId() + "</artifactId><version>" + queueEntry.getVersion() + "</version><packaging>pom</packaging></project>";
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try(ZipOutputStream zos = new ZipOutputStream(baos)) {
				zos.putNextEntry(new ZipEntry("META-INF/maven/" + queueEntry.getGroupId() + "/" + queueEntry.getArtifactId() + "/pom.xml"));
				zos.write(pom.getBytes(Charset.forName("utf-8")));
				zos.closeEntry();
			} catch(IOException ex) {
				throw new RuntimeException(ex);
			}
			dependencies = dependencyService.getDependencies(ProjectType.MAVEN, baos.toByteArray());
		}
		if(dependencies == null || dependencies.isEmpty()) {
			version.setRequires(new ArrayList<>());
		} else {
			List<VersionModel> versionDependencies = new ArrayList<>();
			boolean unresolvedDependencyFound = false;
			
			long start = queueEntry.getPosition() == null ? dependencies.size() : (queueEntry.getPosition() - dependencies.size());
			for(Dependency dependency : dependencies) {
				Optional<VersionModel> optionalDependencyVersion = versionService.getByFullQualifier(dependency.getGroupId(), dependency.getArtifactId(), dependency.getVersion());
				if(optionalDependencyVersion.isPresent()) {
					versionDependencies.add(optionalDependencyVersion.get());
				} else {
					unresolvedDependencyFound = true;
					Optional<QueueEntryModel> optionalDependencyQueueEntry = queueEntryService.getByDependency(dependency.getGroupId(), dependency.getArtifactId(), dependency.getVersion());
					Long position = Long.valueOf(start++);
					if(optionalDependencyQueueEntry.isPresent()) {
						QueueEntryModel dependencyQueueEntry = optionalDependencyQueueEntry.get();
						dependencyQueueEntry.setPosition(position);
						queueEntryService.save(dependencyQueueEntry);
					} else {
						byte[] compiledBytes;
						try {
							compiledBytes = fileService.getBytesFromUrl(dependency.getCompiledUrl());
						} catch (IOException e) {
							LOG.error("could not read bytes from {}", dependency.getCompiledUrl());
							return Boolean.FALSE;
						}
						
						byte[] sourceBytes;
						try {
							sourceBytes = fileService.getBytesFromUrl(dependency.getSourceUrl());
						} catch (IOException e) {
							LOG.error("could not read bytes from {}", dependency.getSourceUrl());
							sourceBytes = new byte[0];
						}
						
						try {
							queueEntryService.importEntry(compiledBytes, sourceBytes, position);
						} catch(IllegalArgumentException e) {
							QueueEntryModel dependencyQueueEntry = new  QueueEntryModel();
							dependencyQueueEntry.setType(ProjectType.PLAIN_JAVA);
							dependencyQueueEntry.setGroupId(dependency.getGroupId());
							dependencyQueueEntry.setArtifactId(dependency.getArtifactId());
							dependencyQueueEntry.setVersion(dependency.getVersion());
							dependencyQueueEntry.setState(QueueEntryState.NEW);
							dependencyQueueEntry.setCompiledData(compiledBytes);
							dependencyQueueEntry.setSourceData(sourceBytes);
							dependencyQueueEntry.setPosition(position);
							queueEntryService.save(dependencyQueueEntry);
						}
						
					}
				}
			}
			
			if(unresolvedDependencyFound) {
				queueEntry.setPosition(Long.valueOf(start + 1));
				queueEntry.setState(QueueEntryState.SKIPPED);
				queueEntryService.save(queueEntry);
				versionService.remove(version.getId());
				if(versionService.getByProjectId(project.getId()).isEmpty()) {
					projectService.remove(project.getId());
				}
				return Boolean.TRUE;
			} else {
				Map<Long, VersionModel> requiredVersions = new HashMap<>(versionDependencies.size());
				for(VersionModel versionDependency : versionDependencies) {
					if(!versionDependency.getProject().getGroupId().equals("java") && !version.getProject().getArtifactId().equals("jdk")) {
						requiredVersions.put(versionDependency.getId(), versionDependency);
					}
				}
				
				version.setRequires(new ArrayList<>(requiredVersions.values()));
			}
		}

		if(!ProjectType.JAVA.equals(queueEntry.getType())) {
			Optional<VersionModel> optionalVersion = dependencyService.getJavaVersion(queueEntry.getCompiledData());
			if(optionalVersion.isPresent()) {
				version.getRequires().add(0, optionalVersion.get());
			} else {
				LOG.warn("no java version found for entry {}.", queueEntry.getId());
				return Boolean.FALSE;
			}
		}
		
		return null;
	}
	
	private List<ClassAnalysis> importClasses(QueueEntryModel queueEntry, Path folder, VersionModel version, Map<String, PackageModel> packagesCache, Map<String, ClassModel> classesCache) {
		List<ClassAnalysis> classAnalysises = new ArrayList<>();
		
		try(
			ByteArrayInputStream bais = new ByteArrayInputStream(queueEntry.getCompiledData());
			ZipInputStream is = new ZipInputStream(bais);
		) {
			ZipEntry zipEntry = null;
			
			StopWatch watch = new StopWatch();
			watch.start();
			
			int counter = 0;
			while(null != (zipEntry = is.getNextEntry())) {
				if(zipEntry.getName().endsWith(".class") && !zipEntry.getName().endsWith("package-info.class")) {
					byte[] bytes = fileService.getBytesFromInputStream(is);
					ClassAnalysis classAnalysis = classAnalyzerService.analyzeClass(bytes);
					classAnalysises.add(classAnalysis);
					
					ClassModel clazz = new ClassModel();
					
					clazz.setName(ClassUtil.extractClassName(classAnalysis.getClassName()));
					clazz.setCompilation(bytes);
					
					Path classPath = folder.resolve(ClassUtil.toFileName(classAnalysis.getClassName()) + ".java");
					if(Files.exists(classPath)) {
						clazz.setSource(Files.readAllBytes(classPath));
					}
					
					String packageName = ClassUtil.extractPackage(classAnalysis.getClassName());
					PackageModel packagez = packagesCache.get(packageName);
					if(packagez == null) {
						packagez = new PackageModel();
						packagez.setName(packageName);
						packagez.setVersion(version);
						packagesCache.put(packageName, packagez);
						packageService.save(packagez);
						
						LOG.debug("created package {} with id {}.", packageName, packagez.getId());
					}
					
					clazz.setPackageModel(packagez);
					classService.save(clazz);
					
					classesCache.put(classAnalysis.getClassName(), clazz);
					
					LOG.debug("created class {} with id {}.", clazz.getName(), clazz.getId());
					counter++;
				}
			}
			LOG.info("imported {} classes after {}ms.", counter, watch.stop());
				
			classImportPostProcessors.forEach(processor -> processor.process(queueEntry.getType(), version, classesCache));
		} catch (IOException e) {
			LOG.error("could not handle QueueEntry with id {}.", queueEntry.getId(), e);
			return null;
		}
		
		return classAnalysises;
	}
	
	private ProjectModel getOrCreateProject(String groupId, String artifactId) {
		Optional<ProjectModel> optionalProject = projectService.getByGroupIdAndArtifactId(groupId, artifactId);
		ProjectModel project;
		if(optionalProject.isPresent()) {
			project = optionalProject.get();
		} else {
			project = new ProjectModel();
			project.setGroupId(groupId);
			project.setArtifactId(artifactId);
			
			LOG.info("created project with id: {}, groupId: {}, artifactId: {}.", project.getId(), project.getGroupId(), project.getArtifactId());
		}
		return project;
	}
	
	private String buildMethodsSignatureCacheKey(Long classId, Long returnTypeId, String signature) {
		return classId + "|||"  + returnTypeId + "|||" + signature;
	}

	/**
	 * Validates if all data is available in the given {@code QueueEntryModel}.
	 * 
	 * @param queueEntry
	 *            The entry to validate
	 * @return {@code true} if the source and the compield data is available
	 *         otherwise it will return {@code false}
	 */
	private boolean isValidData(QueueEntryModel queueEntry) {
		return queueEntry.getCompiledData() != null && queueEntry.getCompiledData().length > 0;
	}

	/**
	 * Sets the {@code QueueEntryService}.
	 * 
	 * @param queueEntryService
	 *            The service
	 */
	public void setQueueEntryService(QueueEntryService queueEntryService) {
		this.queueEntryService = queueEntryService;
	}

	/**
	 * Sets the {@code ProjectService}.
	 * 
	 * @param projectService
	 *            The service
	 */
	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

	/**
	 * Sets the {@code VersionService}.
	 * 
	 * @param versionService
	 *            The service
	 */
	public void setVersionService(VersionService versionService) {
		this.versionService = versionService;
	}

	/**
	 * Sets the {@code ClassService}.
	 * 
	 * @param classService
	 *            The service
	 */
	public void setClassService(ClassService classService) {
		this.classService = classService;
	}

	/**
	 * Sets the {@code DependencyService}.
	 * 
	 * @param dependencyService
	 *            The service
	 */
	public void setDependencyService(DependencyService dependencyService) {
		this.dependencyService = dependencyService;
	}

	/**
	 * Sets the {@code PackageService}.
	 * 
	 * @param packageService
	 *            The service
	 */
	public void setPackageService(PackageService packageService) {
		this.packageService = packageService;
	}

	/**
	 * Sets the {@code ClassAnalyzerService}.
	 * 
	 * @param classAnalyzerService
	 *            The service
	 */
	public void setClassAnalyzerService(ClassAnalyzerService classAnalyzerService) {
		this.classAnalyzerService = classAnalyzerService;
	}

	/**
	 * Sets the {@code FileService}.
	 * 
	 * @param fileService
	 *            The service
	 */
	public void setFileService(FileService fileService) {
		this.fileService = fileService;
	}

	/**
	 * Sets the {@code MethodService}.
	 * 
	 * @param methodService
	 *            The service
	 */
	public void setMethodService(MethodService methodService) {
		this.methodService = methodService;
	}
	
	/**
	 * Sets the collection of {@code ClassImportPostProcessor}s.
	 * @param classImportPostProcessors The post processors
	 */
	public void setClassImportPostProcessors(Collection<ClassImportPostProcessor> classImportPostProcessors) {
		this.classImportPostProcessors = classImportPostProcessors;
	}
	
	public void setCodeSnippedService(CodeSnippedService codeSnippedService) {
		this.codeSnippedService = codeSnippedService;
	}
}
