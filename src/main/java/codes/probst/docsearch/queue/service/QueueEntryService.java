package codes.probst.docsearch.queue.service;

import java.time.Instant;
import java.util.Optional;

import codes.probst.docsearch.queue.model.QueueEntryModel;
import codes.probst.framework.service.Service;

/**
 * Imports and manages {@code QueueEntryModel}s for further processing.
 * @author Benjamin Probst
 *
 */
public interface QueueEntryService extends Service<QueueEntryModel, Long> {

	/**
	 * Returns a {@code QueueEntryModel} if there is one with matching groupId, artifactId and version otherwise {@code Optional#empty()} is returned.
	 * @param groupId The groupId
	 * @param artifactId The artifactId
	 * @param version The version
	 * @return The {@code QueueEntryModel} with the given project infos and version or {@code Optional#empty()} if none matched.
	 */
	public abstract Optional<QueueEntryModel> getByDependency(String groupId, String artifactId, String version);

	/**
	 * Returns the nect {@code QueueEntryModel} to be processed
	 * @return Returns a {@code QueueEntryModel} if there is one to process and {@code Optional#empty()} if nothing is to process.
	 */
	public abstract Optional<QueueEntryModel> getNextQueueEntryToProcess();

	/**
	 * Analyzes the compiled {@code byte}s and retrieves its {@code ProjectInformation} to be imported in a {@code QueueEntryModel}.
	 * @param compiled Project compiled {@code byte}s
	 * @param source Project source {@code byte}s
	 */
	public abstract void importEntry(byte[] compiled, byte[] source);
	
	/**
	 * Analyzes the compiled {@code byte}s and retrieves its {@code ProjectInformation} to be imported in a {@code QueueEntryModel}. 
	 * This method allows a defined position it should be handled.
	 * @param compiled Project compiled {@code byte}s
	 * @param source Project source {@code byte}s
	 * @param position The position the {@code QueueEntryModel} should be handled.
	 */
	public abstract void importEntry(byte[] compiled, byte[] source, Long position);

	/**
	 * Removes all {@code QueueEntryModel}s with state {@code QueueEntryState#SUCCESSFULLY}.
	 * @param instant The reference time
	 */
	public abstract void removeBefore(Instant instant);

}