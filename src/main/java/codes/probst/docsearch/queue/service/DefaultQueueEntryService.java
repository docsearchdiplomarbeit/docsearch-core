package codes.probst.docsearch.queue.service;

import java.time.Instant;
import java.util.Optional;

import codes.probst.docsearch.analyzer.projectinformation.ProjectInformation;
import codes.probst.docsearch.analyzer.projectinformation.ProjectInformationAnalyzer;
import codes.probst.docsearch.queue.dao.QueueEntryDao;
import codes.probst.docsearch.queue.model.QueueEntryModel;
import codes.probst.docsearch.queue.model.QueueEntryState;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;
import codes.probst.framework.pagination.SortDirection;
import codes.probst.framework.service.AbstractService;

/**
 * Implementation of the {@code QueueEntryService} that returns all entries to work that are in state
 * {@code QueueEntryState#SKIPPED} or {@code QueueEntryState#NEW}.
 * Requires a {@code ProjectInformationAnalyzer} to retrieve the {@code ProjectInformation} from the compiled {@code byte}s.
 * @author Benjamin Probst
 *
 */
public class DefaultQueueEntryService extends AbstractService<QueueEntryDao, QueueEntryModel, Long> implements QueueEntryService {
	private static final QueueEntryState[] UNHANDLED_QUEUE_ENTRY_STATES = new QueueEntryState[] { QueueEntryState.SKIPPED, QueueEntryState.NEW };
	
	private ProjectInformationAnalyzer projectInformationAnalyzer;
	
	@Override
	public void save(QueueEntryModel object) {
		object.setModificationDate(Instant.now());
		super.save(object);
	}
	
	/**
	 *  {@inheritDoc}
	 */
	@Override
	public void importEntry(byte[] compiled, byte[] source) {
		importEntry(compiled, source, null);
	}
	
	/**
	 *  {@inheritDoc}
	 */
	@Override
	public void importEntry(byte[] compiled, byte[] source, Long position) {
		if(compiled == null || source == null) {
			throw new IllegalArgumentException("compiledOrSource");
		}
		
		Optional<ProjectInformation> optionalInformation = projectInformationAnalyzer.analyze(compiled);
		if(!optionalInformation.isPresent()) {
			throw new IllegalArgumentException("projectInformation");
		}
		
		ProjectInformation information = optionalInformation.get();
		
		QueueEntryModel queueEntry = new  QueueEntryModel();
		queueEntry.setType(information.getType());
		queueEntry.setGroupId(information.getGroupId());
		queueEntry.setArtifactId(information.getArtifactId());
		queueEntry.setVersion(information.getVersion());
		queueEntry.setState(QueueEntryState.NEW);
		queueEntry.setCompiledData(compiled);
		queueEntry.setSourceData(source);
		queueEntry.setPosition(position);
		
		save(queueEntry);
	}
	
	/**
	 *  {@inheritDoc}
	 */
	@Override
	public Optional<QueueEntryModel> getByDependency(String groupId, String artifactId, String version) {
		return getDao().findFirstByDependencyAndState(groupId, artifactId, version, UNHANDLED_QUEUE_ENTRY_STATES);
	}
	
	/**
	 *  {@inheritDoc}
	 */
	@Override
	public Optional<QueueEntryModel> getNextQueueEntryToProcess() {
		Pageable pageable = new Pageable();
		pageable.setSort(new Sort[] { new Sort("position", SortDirection.ASCENDING) });
		return getDao().findFirstWithState(UNHANDLED_QUEUE_ENTRY_STATES, pageable);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeBefore(Instant instant) {
		getDao().deleteBefore(instant, QueueEntryState.SUCCESSFULLY);
	}
	
	/**
	 * Sets the projectInformationAnalyzer to retrieve the {@code ProjectInformation} from the given {@code byte}s.
	 * @param projectInformationAnalyzer The analyzer
	 */
	public void setProjectInformationAnalyzer(ProjectInformationAnalyzer projectInformationAnalyzer) {
		this.projectInformationAnalyzer = projectInformationAnalyzer;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean isNew(QueueEntryModel object) {
		return object.getId() == null;
	}
}
