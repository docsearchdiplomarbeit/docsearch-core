package codes.probst.docsearch.queue.dao;

import java.time.Instant;
import java.util.Optional;

import codes.probst.docsearch.queue.model.QueueEntryModel;
import codes.probst.docsearch.queue.model.QueueEntryState;
import codes.probst.framework.dao.Dao;
import codes.probst.framework.pagination.Pageable;

/**
 * Data access object to work with {@code QueueEntryModel}s.
 * @author Benjamin Probst
 *
 */
public interface QueueEntryDao extends Dao<QueueEntryModel, Long>{
	
	/**
	 * Returns the first entry with the given {@code QueueEntryState}s.
	 * @param states The states to check
	 * @param pageable The pageable with possible sorts
	 * @return A {@code QueueEntryModel} if one is found or {@code Optional#empty()} it none.
	 */
	public Optional<QueueEntryModel> findFirstWithState(QueueEntryState[] states, Pageable pageable);
	
	/**
	 * Returns the first {@code QueueEntryModel} with the given project informations and the states.
	 * @param groupId The groupId
	 * @param artifactId The artifactId
	 * @param version The version
	 * @param states The states
	 * @return A {@code QueueEntryModel} if one is found or {@code Optional#empty()} it none.
	 */
	public Optional<QueueEntryModel> findFirstByDependencyAndState(String groupId, String artifactId, String version, QueueEntryState... states);

	/**
	 * Removes all entries before the given instant with the given {@code QueueEntryState}s.
	 * @param instant The reference instant
	 * @param states The states to handle
	 */
	public void deleteBefore(Instant instant, QueueEntryState... states);
	
}
