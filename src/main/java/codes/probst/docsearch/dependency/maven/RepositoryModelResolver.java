package codes.probst.docsearch.dependency.maven;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.model.Parent;
import org.apache.maven.model.Repository;
import org.apache.maven.model.building.ModelSource;
import org.apache.maven.model.building.StringModelSource;
import org.apache.maven.model.building.UrlModelSource;
import org.apache.maven.model.resolution.InvalidRepositoryException;
import org.apache.maven.model.resolution.ModelResolver;
import org.apache.maven.model.resolution.UnresolvableModelException;

@SuppressWarnings("deprecation")
public class RepositoryModelResolver implements ModelResolver {
    private List<Repository> repositories = new ArrayList<Repository>();

	public ModelSource resolveModel(String groupId, String artifactId, String versionId) throws UnresolvableModelException {
    	for (Repository repository : repositories) {
    		 String repository1Url = repository.getUrl();
             if (repository1Url.endsWith( "/" )) {
                 repository1Url = repository1Url.substring(0, repository1Url.length() - 1);
             }
             StringBuilder relativePomPath = new StringBuilder("/");
             String[] parts = groupId.split( "\\." );
             for(String part  : parts) {
            	 relativePomPath.append(part);
            	 relativePomPath.append("/");
             }

             relativePomPath.append(artifactId);
             relativePomPath.append("/");
             relativePomPath.append(versionId);
             relativePomPath.append("/");
             relativePomPath.append(artifactId);
             relativePomPath.append("-");
             relativePomPath.append(versionId);
             relativePomPath.append(".pom");
             
             
             try {
				URL url = new URL(repository1Url + relativePomPath.toString());
				
				URLConnection connection = url.openConnection();
				if((connection instanceof HttpURLConnection)) {
	                HttpURLConnection conn = (HttpURLConnection) connection;
	                conn.setInstanceFollowRedirects( true );
	                conn.connect();
	                if(conn.getResponseCode() == 200) {
	                	return new UrlModelSource(url);
	                }
				}
			} catch (IOException e) {
				throw new UnresolvableModelException( "Could not check POM for groupId: " + groupId + ", artifactId: " + artifactId + ", version: " + versionId + ".", groupId, artifactId, versionId);
			}
        }
    	return new StringModelSource("<project><modelVersion>4.0.0</modelVersion><groupId>" + groupId + "</groupId><artifactId>" + artifactId + "</artifactId><version>" + versionId + "</version><packaging>pom</packaging></project>");
    }

    public void addRepository(Repository repository) throws InvalidRepositoryException
    {
        for(Repository existingRepository : repositories ) {
            if(existingRepository.getId().equals( repository.getId())) {
                return;
            }
        }
        
        repositories.add(repository);
    }

    public ModelResolver newCopy()
    {
    	RepositoryModelResolver resolver = new RepositoryModelResolver();
    	resolver.setRepositories(repositories);
    	return resolver;
    }

	@Override
	public ModelSource resolveModel(Parent parent) throws UnresolvableModelException {
		return resolveModel(parent.getGroupId(), parent.getArtifactId(), parent.getVersion());
	}

	@Override
	public void addRepository(Repository paramRepository, boolean paramBoolean) throws InvalidRepositoryException {
		repositories.add(paramRepository);
	}
	
	public void setRepositories(List<Repository> repositories) {
		this.repositories = repositories;
	}
}