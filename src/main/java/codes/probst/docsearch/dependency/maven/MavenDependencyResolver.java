package codes.probst.docsearch.dependency.maven;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.maven.model.Model;
import org.apache.maven.model.building.DefaultModelBuilderFactory;
import org.apache.maven.model.building.DefaultModelBuildingRequest;
import org.apache.maven.model.building.ModelBuilder;
import org.apache.maven.model.building.ModelBuildingException;
import org.apache.maven.model.building.ModelBuildingRequest;
import org.apache.maven.model.building.ModelBuildingResult;
import org.apache.maven.model.building.ModelSource;
import org.apache.maven.model.building.StringModelSource;
import org.apache.maven.model.resolution.ModelResolver;
import org.apache.maven.model.resolution.UnresolvableModelException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codes.probst.docsearch.dependency.Dependency;
import codes.probst.docsearch.dependency.DependencyResolver;
import codes.probst.framework.file.service.FileService;


@SuppressWarnings("deprecation")
public class MavenDependencyResolver implements DependencyResolver {
	private static final Logger LOG = LoggerFactory.getLogger(MavenDependencyResolver.class);
	
	private ModelResolver modelResolver;
	private FileService fileService;
	
	@Override
	public Collection<Dependency> resolve(byte[] bytes) {
		byte[] pomBytes = getPomBytes(bytes);
		if(pomBytes == null) {
			throw new IllegalArgumentException("no pom found");
		}
		
		Model model = parseModel(new StringModelSource(new String(pomBytes, Charset.forName("utf-8"))));
		List<Dependency> dependencies = new ArrayList<>();
		try {
			resolve(modelResolver.resolveModel(model.getGroupId(), model.getArtifactId(), model.getVersion()), dependencies);
		} catch (UnresolvableModelException e1) {
			resolve(new StringModelSource(new String(pomBytes, Charset.forName("utf-8"))), dependencies);
		}
		dependencies.forEach(dependency -> {
			ModelSource modelSource;
			try {
				modelSource = modelResolver.resolveModel(dependency.getGroupId(), dependency.getArtifactId(), dependency.getVersion());
				String location = modelSource.getLocation();
				location = location.substring(0, location.lastIndexOf("/") + 1);
				dependency.setCompiledUrl(new URL(location + dependency.getArtifactId() + "-" + dependency.getVersion() + ".jar"));
				dependency.setSourceUrl(new URL(location + dependency.getArtifactId() + "-" + dependency.getVersion() + "-sources.jar"));
			} catch (UnresolvableModelException e) {
				LOG.error("could not load model source for groupId: {}, artifactId: {}, version: {} after found the dependency.", dependency.getGroupId(), dependency.getArtifactId(), dependency.getVersion(), e);
			} catch (MalformedURLException e) {
				LOG.error("could not create url.", e);
			}
		});
		
		Collections.reverse(dependencies);
		
		return dependencies;
	}
	
	private byte[] getPomBytes(byte[] bytes) {
		try(ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
			ZipInputStream is = new ZipInputStream(bais)) {
			
			for(ZipEntry entry = null; (entry = is.getNextEntry()) != null;) {
				if(entry.getName().startsWith("META-INF/maven") && entry.getName().endsWith("/pom.xml")) {
					return fileService.getBytesFromInputStream(is);
				}
			}
		} catch (IOException e) {
			throw new IllegalArgumentException("could nor read zip", e);
		}
		
		return null;
	}
	
	private Model parseModel(ModelSource modelSource) {
		ModelBuildingRequest request = new DefaultModelBuildingRequest();
		request.setProcessPlugins(false);
		request.setModelSource(modelSource);
		request.setModelResolver(modelResolver);
		request.setTwoPhaseBuilding(true);
		request.setValidationLevel(ModelBuildingRequest.VALIDATION_LEVEL_MINIMAL);
		
		ModelBuilder builder = new DefaultModelBuilderFactory().newInstance();
		try {
			ModelBuildingResult result = builder.build(request);
			Model model = result.getEffectiveModel();
			
			return model;
		} catch (ModelBuildingException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void resolve(ModelSource modelSource, Collection<Dependency> dependencies) {
		Model model = parseModel(modelSource);
			
		for(org.apache.maven.model.Dependency dependency : getAllDependencies(model)) {
			String groupId = dependency.getGroupId() == null ? model.getGroupId() : dependency.getGroupId();
			String artifactId = dependency.getArtifactId();
			String version = dependency.getVersion();
			if(version == null) {
				Optional<org.apache.maven.model.Dependency> optional = model.getDependencyManagement().getDependencies().stream().filter(d -> d.getArtifactId().equals(artifactId) && d.getGroupId().equals(groupId)).findAny();
				if(optional.isPresent()) {
					version = optional.get().getVersion();
				} else {
					version = model.getVersion();
				}
			}
			
			Dependency appDependency = new Dependency(groupId, artifactId, version);
			if(!dependencies.contains(appDependency)) {
				dependencies.add(appDependency);
//					no recursion needed
//					try {
//						resolve(modelResolver.resolveModel(groupId, artifactId, version), dependencies);
//					} catch (UnresolvableModelException e) {
//						LOG.warn("could not resolve model for {}:{}:{}.", groupId, artifactId, version);
//					}
			}
		}
	}
	
	protected List<org.apache.maven.model.Dependency> getAllDependencies(Model model) {
		List<org.apache.maven.model.Dependency> dependencies = new ArrayList<>();
		
		if(model.getDependencies() != null) {
			dependencies.addAll(model.getDependencies());
		}
		
		return dependencies.stream().filter(dependency -> dependency.getScope() == null || !"test".equals(dependency.getScope()) && !"system".equals(dependency.getScope())).collect(Collectors.toList());
	}
	
	public void setModelResolver(ModelResolver modelResolver) {
		this.modelResolver = modelResolver;
	}
	
	public void setFileService(FileService fileService) {
		this.fileService = fileService;
	}
}
