package codes.probst.docsearch.dependency.service;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.analyzer.projectinformation.ProjectType;
import codes.probst.docsearch.dependency.Dependency;
import codes.probst.docsearch.version.model.VersionModel;

public interface DependencyService {

	public Collection<Dependency> getDependencies(ProjectType type, byte[] bytes);
	
	public Optional<VersionModel> getJavaVersion(byte[] bytes);

}