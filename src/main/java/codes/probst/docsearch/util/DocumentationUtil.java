package codes.probst.docsearch.util;

import org.jsoup.Jsoup;

public class DocumentationUtil {
	private static final String SENTENCE_DELIMITER = ".";
	
	private DocumentationUtil() {}
	
	public static String getDocumentationPart(String string, int sentences) {
		if(string == null) {
			string = "";
		} else {
			int position = -1;
			for(int i = 0; i < sentences; i++) {
				int newPosition = string.indexOf(SENTENCE_DELIMITER, position + 1);
				if(newPosition == -1) {
					break;
				}
				if(string.length() > newPosition + 1 && !Character.isWhitespace(string.charAt(newPosition + 1)) && string.charAt(newPosition + 1) != '<') {
					i--;
				}
				position = newPosition;
			}
				
			string = string.substring(0, position + 1);
			
			string = Jsoup.parse(string).body().html();
		}
		
		return string;
	}
}
