package codes.probst.docsearch.project.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import codes.probst.docsearch.version.model.VersionModel;

/**
 * Representation of a hole project.
 * @author Benjamin Probst
 *
 */
@Entity
@Table(name = "PROJECT")
@SequenceGenerator(initialValue = 0, name = "ProjectSequence", sequenceName="PROJECT_SEQ")
public class ProjectModel {
	@Id
	@GeneratedValue(generator = "ProjectSequence", strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	@Column(name = "groupId", nullable = false, length = 255)
	private String groupId;
	@Column(name = "artifactId", nullable = false, length = 255)
	private String artifactId;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "project")
	private List<VersionModel> versions;

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Returns the groupId.
	 * @return The groupId
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * Sets the groupId.
	 * @param groupId The groupId
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * Returns the artifactId.
	 * @return The artifactId
	 */
	public String getArtifactId() {
		return artifactId;
	}

	/**
	 * Sets the artifactId.
	 * @param artifactId The artifactId
	 */
	public void setArtifactId(String artifactId) {
		this.artifactId = artifactId;
	}

	/**
	 * Returns all versions this project contains.
	 * @return All versions
	 */
	public List<VersionModel> getVersions() {
		return versions;
	}
	
	/**
	 * Sets all versions this project contains.
	 * @param versions all versions
	 */
	public void setVersions(List<VersionModel> versions) {
		this.versions = versions;
	}

}
