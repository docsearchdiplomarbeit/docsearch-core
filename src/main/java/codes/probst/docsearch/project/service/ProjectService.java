package codes.probst.docsearch.project.service;

import java.util.Optional;

import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.framework.service.Service;

/**
 * Service to work with {@code ProjectModel}s.
 * @author Benjamin Probst
 *
 */
public interface ProjectService extends Service<ProjectModel, Long> {

	/**
	 * Returns a {@code ProjectModel} if one matches the given groupId and artifactId.
	 * @param groupId The groupId.
	 * @param artifactId The artifactId.
	 * @return A {@code ProjectModel} if one matches the given groupId and artifactId otherwise {@code Optional#empty()}
	 */
	public abstract Optional<ProjectModel> getByGroupIdAndArtifactId(String groupId, String artifactId);

	public abstract Optional<ProjectModel> getByClassId(Long classId);

	public abstract Optional<ProjectModel> getByVersionId(Long versionId);

}