package codes.probst.docsearch.project.service;

import java.util.Optional;

import codes.probst.docsearch.project.dao.ProjectDao;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.framework.service.AbstractService;

/**
 * Default implementation of the {@code ProjectService} interface backed by a {@code ProjectDao}.
 * @author Benjamin Probst
 *
 */
public class DefaultProjectService extends AbstractService<ProjectDao, ProjectModel, Long> implements ProjectService {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<ProjectModel> getByGroupIdAndArtifactId(String groupId, String artifactId) {
		return getDao().findByGroupIdAndArtifactId(groupId, artifactId);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<ProjectModel> getByClassId(Long classId) {
		return getDao().findByClassId(classId);
	}
	
	@Override
	public Optional<ProjectModel> getByVersionId(Long versionId) {
		return getDao().findByVersionId(versionId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean isNew(ProjectModel object) {
		return object.getId() == null;
	}
	
}
