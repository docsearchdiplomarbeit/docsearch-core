package codes.probst.docsearch.project.facade;

import java.util.Optional;

import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.project.service.ProjectService;
import codes.probst.framework.facade.AbstractFacade;

public class DefaultProjectFacade extends AbstractFacade<ProjectModel, ProjectData, ProjectService, Long> implements ProjectFacade {
	
	@Override
	public Optional<ProjectData> getByClassId(Long classId, ProjectPopulationOption... options) {
		Optional<ProjectModel> optionalProject = getService().getByClassId(classId);
		
		if(optionalProject.isPresent()) {
			return Optional.of(convertAndPopulate(optionalProject.get(), options));
		}
		
		return Optional.empty();
	}
	
	@Override
	public Optional<ProjectData> getByVersionId(Long versionId, ProjectPopulationOption... options) {
		Optional<ProjectModel> optionalProject = getService().getByVersionId(versionId);
		
		if(optionalProject.isPresent()) {
			return Optional.of(convertAndPopulate(optionalProject.get(), options));
		}
		
		return Optional.empty();
	}
	
}
