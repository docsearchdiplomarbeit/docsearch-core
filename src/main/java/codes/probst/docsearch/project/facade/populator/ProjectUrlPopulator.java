package codes.probst.docsearch.project.facade.populator;

import codes.probst.docsearch.project.facade.ProjectData;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.framework.populator.Populator;
import codes.probst.framework.url.UrlResolver;

public class ProjectUrlPopulator implements Populator<ProjectModel, ProjectData> {
	private UrlResolver<ProjectModel> resolver;
	
	@Override
	public void populate(ProjectModel source, ProjectData target) {
		target.setUrl(resolver.resolve(source));
	}

	public void setResolver(UrlResolver<ProjectModel> resolver) {
		this.resolver = resolver;
	}
}
