package codes.probst.docsearch.project.facade.populator;

import codes.probst.docsearch.project.facade.ProjectData;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.facade.VersionFacade;
import codes.probst.docsearch.version.facade.VersionPopulationOption;
import codes.probst.framework.bean.BeanResolver;
import codes.probst.framework.populator.Populator;

public class ProjectVersionsPopulator implements Populator<ProjectModel, ProjectData> {
	private BeanResolver beanResolver;
	private VersionFacade versionFacade;
	
	@Override
	public void populate(ProjectModel source, ProjectData target) {
		target.setVersions(getVersionFacade().getByProjectId(source.getId(), VersionPopulationOption.OPTION_URL));
	}
	
	private VersionFacade getVersionFacade() {
		if(versionFacade == null) {
			versionFacade = beanResolver.resolve(VersionFacade.class);
		}
		return versionFacade;
	}

	public void setBeanResolver(BeanResolver beanResolver) {
		this.beanResolver = beanResolver;
	}
}
