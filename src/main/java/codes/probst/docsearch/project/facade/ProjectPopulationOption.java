package codes.probst.docsearch.project.facade;

import codes.probst.framework.populator.PopulationOption;

public class ProjectPopulationOption extends PopulationOption {
	public static final ProjectPopulationOption OPTION_VERSIONS = new ProjectPopulationOption("VERSIONS", 1);
	public static final ProjectPopulationOption OPTION_URL = new ProjectPopulationOption("URL", 1);
	
	protected ProjectPopulationOption(String code, int order) {
		super("PROJECT_POPULATION_OPTION_" + code, order);
	}

	
}
