package codes.probst.docsearch.project.dao;

import java.util.Optional;

import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.framework.dao.Dao;

/**
 * Data access for {@code ProjectModel}s.
 * @author Benjamin Probst
 *
 */
public interface ProjectDao extends Dao<ProjectModel, Long> {

	/**
	 * Returns a {@code ProjectModel} based on the given groupId and artifactId.
	 * @param groupId The groupId
	 * @param artifactId The artifactId
	 * @return A {@code ProjectModel} based on the given groupId and artifactId or {@code Optional#empty()} if none found.
	 */
	public abstract Optional<ProjectModel> findByGroupIdAndArtifactId(String groupId, String artifactId);

	public abstract Optional<ProjectModel> findByClassId(Long classId);

	public abstract Optional<ProjectModel> findByVersionId(Long versionId);
	
}
