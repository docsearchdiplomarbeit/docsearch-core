package codes.probst.docsearch.project.dao;

import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.model.ClassModel_;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.packages.model.PackageModel_;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.project.model.ProjectModel_;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.docsearch.version.model.VersionModel_;
import codes.probst.framework.dao.AbstractJpaDao;

/**
 * Implementation of the {@code ProjectDao} interface based on the java persistence framework.
 * @author Benjamin Probst
 *
 */
public class JpaProjectDao extends AbstractJpaDao<ProjectModel, Long> implements ProjectDao {

	public JpaProjectDao() {
		super(ProjectModel.class, ProjectModel_.id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<ProjectModel> findByGroupIdAndArtifactId(String groupId, String artifactId) {

		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<ProjectModel> criteria = builder.createQuery(ProjectModel.class);
		Root<ProjectModel> root = criteria.from(ProjectModel.class);
		
		criteria.where(
			builder.and(
				builder.equal(root.get(ProjectModel_.groupId), groupId),
				builder.equal(root.get(ProjectModel_.artifactId), artifactId)
			)	
		);
		
		return selectOnce(criteria);
	}

	@Override
	public Optional<ProjectModel> findByClassId(Long classId) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<ProjectModel> criteria = builder.createQuery(ProjectModel.class);
		Root<ProjectModel> root = criteria.from(ProjectModel.class);
		
		Join<ProjectModel, VersionModel> versionJoin = root.join(ProjectModel_.versions);
		Join<VersionModel, PackageModel> packageJoin = versionJoin.join(VersionModel_.packages);
		Join<PackageModel, ClassModel> classJoin = packageJoin.join(PackageModel_.classes);
		
		criteria.where(
			builder.equal(classJoin.get(ClassModel_.id), classId)
		);
		return selectOnce(criteria);
	}
	
	@Override
	public Optional<ProjectModel> findByVersionId(Long versionId) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<ProjectModel> criteria = builder.createQuery(ProjectModel.class);
		Root<ProjectModel> root = criteria.from(ProjectModel.class);

		Join<ProjectModel, VersionModel> join = root.join(ProjectModel_.versions);
		criteria.where(
			builder.equal(join.get(VersionModel_.id), versionId)
		);
		return selectOnce(criteria);
	}
	
}
