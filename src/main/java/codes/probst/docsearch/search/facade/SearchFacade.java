package codes.probst.docsearch.search.facade;

import java.util.Collection;
import java.util.Map;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.framework.pagination.Pageable;

public interface SearchFacade {

	public SearchResultData search(String searchQuery, Map<String, Map<String, String>> filters, Pageable pageable);

	public Collection<ClassData> getRandomClasses(Pageable pageable);
	
}