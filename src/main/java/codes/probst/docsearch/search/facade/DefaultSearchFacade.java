package codes.probst.docsearch.search.facade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.project.facade.ProjectData;
import codes.probst.docsearch.search.MultiSearchResult;
import codes.probst.docsearch.search.SearchResult;
import codes.probst.docsearch.search.service.SearchService;
import codes.probst.docsearch.util.DocumentationUtil;
import codes.probst.docsearch.version.facade.VersionData;
import codes.probst.framework.pagination.Pageable;

public class DefaultSearchFacade implements SearchFacade {
	private SearchService searchService;
	
	@Override
	public SearchResultData search(String searchQuery, Map<String, Map<String, String>> filters, Pageable pageable) {
		MultiSearchResult result = searchService.search(searchQuery, filters, pageable);
		
		SearchResultData data = new SearchResultData();
		data.setSearchText(searchQuery);
		processProjectResult(result, data);
		processPackageResult(result, data);
		processClassResult(result, data);
		
		return data;
	}
	
	@Override
	public Collection<ClassData> getRandomClasses(Pageable pageable) {
		MultiSearchResult result = searchService.getRandomClass(pageable);
		
		SearchResultData data = new SearchResultData();
		processClassResult(result, data);
		
		return data.getClasses();
	}
	
	private void processProjectResult(MultiSearchResult result, SearchResultData data) {
		SearchResult projectResult = result.getResult("project");
		if(projectResult == null) {
			data.setTotalProjects(0);
			data.setProjects(Collections.emptyList());
			data.setProjectFilters(Collections.emptyMap());
		} else {
			data.setTotalProjects(projectResult.getTotal());
			data.setProjects(processProjects(projectResult));
			data.setProjectFilters(projectResult.getFilters());
		}
	}
	
	private void processClassResult(MultiSearchResult result, SearchResultData data) {
		SearchResult classResult = result.getResult("class");
		if(classResult == null) {
			data.setTotalClasses(0);
			data.setClasses(Collections.emptyList());
			data.setClassFilters(Collections.emptyMap());
		} else {
			data.setTotalClasses(classResult.getTotal());
			data.setClasses(processClasses(classResult));
			data.setClassFilters(classResult.getFilters());
		}
	}
	
	private void processPackageResult(MultiSearchResult result, SearchResultData data) {
		SearchResult packageResult = result.getResult("package");
		if(packageResult == null) {
			data.setTotalPackages(0);
			data.setPackages(Collections.emptyList());
			data.setClassFilters(Collections.emptyMap());
		} else {
			data.setTotalPackages(packageResult.getTotal());
			data.setPackages(processPackages(packageResult));
			data.setClassFilters(packageResult.getFilters());
		}
	}
	
	private Collection<ClassData> processClasses(SearchResult result) {
		Collection<ClassData> classes = new ArrayList<>(result.getValues().size());
		for(Map<String, List<Object>> value : result.getValues()) {
			ClassData data = new ClassData();
			data.setName(getValue(value, "name"));
			data.setDescription(DocumentationUtil.getDocumentationPart(getValue(value, "documentation"), 1));
			data.setUrl(getValue(value, "url"));
			
			ProjectData project = new ProjectData();
			project.setUrl(getValue(value, "project.url"));
			project.setArtifactId(getValue(value, "project.artifactId"));
			project.setGroupId(getValue(value, "project.groupId"));

			VersionData version = new VersionData();
			version.setUrl(getValue(value, "version.url"));
			version.setCode(getValue(value, "version.code"));
			
			data.setVersion(version);
			data.setProject(project);
			
			classes.add(data);
		}
		return classes;
	}
	
	private Collection<PackageData> processPackages(SearchResult result) {
		Collection<PackageData> packages = new ArrayList<>(result.getValues().size());
		for(Map<String, List<Object>> value : result.getValues()) {
			PackageData data = new PackageData();
			data.setName(getValue(value, "name"));
			data.setDocumentation(DocumentationUtil.getDocumentationPart(getValue(value, "documentation"), 1));
			data.setUrl(getValue(value, "url"));
			
			ProjectData project = new ProjectData();
			project.setUrl(getValue(value, "project.url"));
			project.setArtifactId(getValue(value, "project.artifactId"));
			project.setGroupId(getValue(value, "project.groupId"));

			VersionData version = new VersionData();
			version.setUrl(getValue(value, "version.url"));
			version.setCode(getValue(value, "version.code"));
			
			data.setVersion(version);
			version.setProject(project);
			
			packages.add(data);
		}
		return packages;
	}
	
	private Collection<ProjectData> processProjects(SearchResult result) {
		Collection<ProjectData> projects = new ArrayList<>(result.getValues().size());
		for(Map<String, List<Object>> value : result.getValues()) {
			ProjectData data = new ProjectData();
			data.setGroupId(getValue(value, "groupId"));
			data.setArtifactId(getValue(value, "artifactId"));
			data.setUrl(getValue(value, "url"));
			
			VersionData version = new VersionData();
			version.setUrl(getValue(value, "version.url"));
			version.setCode(getValue(value, "version.code"));
			
			data.setVersions(Arrays.asList(version));
			
			projects.add(data);
		}
		return projects;
	}
	
	private <T> T getValue(Map<String, List<Object>> map, String key) {
		List<T> values = getValues(map, key);
		return values == null || values.isEmpty() ? null : values.get(0);
	}
	
	@SuppressWarnings("unchecked")
	private <T> List<T> getValues(Map<String, List<Object>> map, String key) {
		return (List<T>) map.get(key);
	}
	
	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
}
