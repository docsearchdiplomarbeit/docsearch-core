package codes.probst.docsearch.search.job;


import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codes.probst.docsearch.search.index.IndexStrategy;
import codes.probst.docsearch.search.index.IndexType;
import codes.probst.framework.monitoring.StopWatch;

@DisallowConcurrentExecution
public class SearchIndexJob implements Job {
	private static final Logger LOG = LoggerFactory.getLogger(SearchIndexJob.class);
	
	private IndexStrategy strategy;
	private IndexType type;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch watch = new StopWatch();
		watch.start();
		try {
			strategy.index(type);
		} catch(Exception e) {
			LOG.error("could not index.", e);
		} finally {
			LOG.info("index finished after {}ms.", watch.stop());
		}
		
	}
	
	public void setStrategy(IndexStrategy strategy) {
		this.strategy = strategy;
	}
	
	public void setType(IndexType type) {
		this.type = type;
	}
	
}
