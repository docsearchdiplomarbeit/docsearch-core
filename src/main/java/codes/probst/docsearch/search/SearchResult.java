package codes.probst.docsearch.search;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class SearchResult {
	private Collection<Map<String, List<Object>>> values;
	private Map<String, List<String>> filters;
	private long total;
	
	public SearchResult(Collection<Map<String, List<Object>>> values, long total, Map<String, List<String>> filters) {
		this.values = values;
		this.total = total;
		this.filters = filters;
	}
	
	public Collection<Map<String, List<Object>>> getValues() {
		return values;
	}
	
	public long getTotal() {
		return total;
	}
	
	public Map<String, List<String>> getFilters() {
		return filters;
	}
	
}
