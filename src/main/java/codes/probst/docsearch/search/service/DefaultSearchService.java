package codes.probst.docsearch.search.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.MultiSearchResponse.Item;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHitField;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.Suggest.Suggestion;
import org.elasticsearch.search.suggest.Suggest.Suggestion.Entry.Option;
import org.elasticsearch.search.suggest.term.TermSuggestionBuilder;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.search.MultiSearchResult;
import codes.probst.docsearch.search.SearchResult;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.url.UrlResolver;

public class DefaultSearchService implements SearchService {
	private static final Logger LOG = LoggerFactory.getLogger(DefaultSearchService.class);
	
	private UrlResolver<ProjectModel> projectUrlResolver;
	private UrlResolver<VersionModel> versionUrlResolver;
	private UrlResolver<PackageModel> packageUrlResolver;
	private UrlResolver<ClassModel> classUrlResolver;
	private Client client;
	
	public MultiSearchResult getRandomClass(Pageable pageable) {
		SearchResponse response = client.prepareSearch("docsearch")
			.setTypes("class")
			.setSearchType(SearchType.QUERY_THEN_FETCH)
			.setFrom(pageable.getCurrentPage())
			.setSize(pageable.getPageSize())
			.addFields("name", "package", "documentation", "url", "version.code", "version.url", "project.artifactId", "project.groupId", "project.url")
			.setQuery(new FunctionScoreQueryBuilder(
					QueryBuilders.boolQuery()
					.must(QueryBuilders.existsQuery("documentation"))
					.mustNot(QueryBuilders.wildcardQuery("name", "*$*"))
				)
				.add(ScoreFunctionBuilders.randomFunction(Instant.now().getMillis()))
			).execute()
			.actionGet();
		
		Map<String, SearchResult> results = new HashMap<>(1);
		handleResponse(results, response);
		
		return new MultiSearchResult(results);
	}
	
	public MultiSearchResult search(String text, Map<String, Map<String, String>> filters, Pageable pageable) {
		SearchRequestBuilder projectSearchRequestBuilder = client.prepareSearch("docsearch")
			.setQuery(
				QueryBuilders.boolQuery()
					.should(QueryBuilders.matchQuery("groupId", text))
					.should(QueryBuilders.matchQuery("artifactId", text))
					.should(QueryBuilders.matchQuery("version.code", text))
			)
			.setTypes("project")
			.addFields("groupId", "artifactId", "version.code", "version.url")
			.addSuggestion(new TermSuggestionBuilder("suggestion").text(text).field("artifactId"))
			.setSearchType(SearchType.QUERY_THEN_FETCH)
			.setFrom(pageable.getCurrentPage())
			.setSize(pageable.getPageSize());
		
		SearchRequestBuilder packageSearchRequestBuilder = 
			client.prepareSearch("docsearch")
				.setQuery(
					QueryBuilders.boolQuery()
						.should(QueryBuilders.matchQuery("name", text).boost(2f))
						.should(QueryBuilders.matchQuery("documentation", text))
				)
				.setTypes("package")
				.addAggregation(AggregationBuilders.terms("artifactIds").field("project.artifactId"))
				.addFields("name", "documentation", "url", "version.code", "version.url", "project.artifactId", "project.groupId", "project.url")
				.addSuggestion(new TermSuggestionBuilder("suggestion").text(text).field("name"))
				.setSearchType(SearchType.QUERY_THEN_FETCH)
				.setFrom(pageable.getCurrentPage())
				.setSize(pageable.getPageSize());
		
		SearchRequestBuilder classSearchRequestBuilder = client.prepareSearch("docsearch")
				.setQuery(
					QueryBuilders.boolQuery()
						.should(QueryBuilders.matchQuery("name", text).boost(2f))
						.should(QueryBuilders.matchQuery("package", text).boost(2f))
						.should(QueryBuilders.matchQuery("fullQualifiedName", text).boost(4f))
						.should(QueryBuilders.matchQuery("documentation", text))
						.should(QueryBuilders.matchQuery("methods.name", text).boost(2f))
						.should(QueryBuilders.matchQuery("methods.documentation", text))
				)
				.setTypes("class")
				.addAggregation(AggregationBuilders.terms("artifactIds").field("project.artifactId"))
				.addFields("name", "package", "documentation", "url", "version.code", "version.url", "project.artifactId", "project.groupId", "project.url")
				.addSuggestion(new TermSuggestionBuilder("suggestion").text(text).field("name"))
				.setSearchType(SearchType.QUERY_THEN_FETCH)
				.setFrom(pageable.getCurrentPage())
				.setSize(pageable.getPageSize());
		
		
		if(filters.containsKey("class")) {
			String artifactId = filters.get("class").get("artifactIds");
			if(artifactId != null) {
				classSearchRequestBuilder.setPostFilter(QueryBuilders.termQuery("project.artifactId", artifactId));
			}
		}
		if(filters.containsKey("package")) {
			String artifactId = filters.get("package").get("artifactIds");
			if(artifactId != null) {
				packageSearchRequestBuilder.setPostFilter(QueryBuilders.termQuery("project.artifactId", artifactId));
			}
		}
		
		MultiSearchResponse response = client.prepareMultiSearch()
			.add(projectSearchRequestBuilder)
			.add(packageSearchRequestBuilder)
			.add(classSearchRequestBuilder)
			.execute()
			.actionGet();
		
		
		Map<String, SearchResult> results = new HashMap<>(response.getResponses().length);
		for(Item item : response.getResponses()) {
			handleResponse(results, item.getResponse());
		}
		
		MultiSearchResult result = new MultiSearchResult(results);
		if(result.getTotal() == 0) {
			float bestFloat = 0f;
			String bestValue = null;
			
			for(Item item : response.getResponses()) {
				Suggest suggest = item.getResponse().getSuggest();
				if(suggest != null) {
					for(Suggestion<? extends org.elasticsearch.search.suggest.Suggest.Suggestion.Entry<? extends Option>> suggestion : suggest) {
						for(org.elasticsearch.search.suggest.Suggest.Suggestion.Entry<? extends Option> entry : suggestion.getEntries()) {
							for(Option option : entry.getOptions()) {
								if(bestFloat < option.getScore()) {
									bestFloat = option.getScore();
									bestValue = option.getText().string();
								}
							}
						}
					}
				}
			}
			
			if(bestValue != null) {
				return search(bestValue, filters, pageable);
			}			
		}
		return result;
	}
	
	private void handleResponse(Map<String, SearchResult> results, SearchResponse response) {
		String type = null;
		
		Map<String, List<String>> filters = new HashMap<>();
		Collection<Map<String, List<Object>>> values = new ArrayList<>(response.getHits().getHits().length);
		for(SearchHit hit : response.getHits()) {
			Map<String, List<Object>> map = new HashMap<>();
			type = hit.getType();
			for(Entry<String, SearchHitField> entry : hit.getFields().entrySet()) {
				SearchHitField field = entry.getValue();
				map.put(field.getName(), field.getValues());
			}
			values.add(map);
		}
		
		if(response.getAggregations() != null) {
			for(Iterator<Aggregation> it = response.getAggregations().iterator(); it.hasNext();) {
				Aggregation aggregation = it.next();
				StringTerms terms = (StringTerms) aggregation;
				filters.put(aggregation.getName(), terms.getBuckets().stream().map(bucket -> {
					return bucket.getKeyAsString();
				}).collect(Collectors.toList()));
			}
		}
		
		results.put(type, new SearchResult(values, response.getHits().getTotalHits(), filters));
	}
	
	@Override
	public void index(ClassModel clazz) {
		Map<String, Object> document = new HashMap<>();
		
		document.put("name", clazz.getName().replace("$", "."));
		document.put("package", clazz.getPackageModel().getName());
		document.put("url", classUrlResolver.resolve(clazz));
		document.put("fullQualifiedName", clazz.getPackageModel().getName().isEmpty() ? clazz.getName().replace("$", ".") : (clazz.getPackageModel().getName() + "." + clazz.getName().replace("$", ".")));
		document.put("documentation", clazz.getDocumentation());
		document.put("methods", clazz.getMethods().stream().map(this::buildMethod).collect(Collectors.toList()));
		document.put("version", buildVersion(clazz.getPackageModel().getVersion()));
		document.put("project", buildProject(clazz.getPackageModel().getVersion().getProject()));

		index("docsearch", "class", clazz.getId().toString(), document);
	}
	
	@Override
	public void index(PackageModel packagez) {
		Map<String, Object> document = new HashMap<>();
		
		document.put("name", packagez.getName());
		document.put("documentation", packagez.getDocumentation());
		document.put("url", packageUrlResolver.resolve(packagez));
		document.put("version", buildVersion(packagez.getVersion()));
		document.put("project", buildProject(packagez.getVersion().getProject()));

		index("docsearch", "package", packagez.getId().toString(), document);
	}
	
	@Override
	public void index(ProjectModel project) {
		Map<String, Object> document = new HashMap<>();
		
		document.put("groupId", project.getGroupId());
		document.put("artifactId", project.getArtifactId());
		document.put("url", projectUrlResolver.resolve(project));
		
		Optional<VersionModel> optionalVersion = project.getVersions().stream().sorted((a, b) -> {
			 String[] vals1 = a.getCode().replaceAll("[^0-9\\.]*", "").split("\\.");
		    String[] vals2 = b.getCode().replaceAll("[^0-9\\.]*", "").split("\\.");
		    int i = 0;
		    // set index to first non-equal ordinal or length of shortest version string
		    while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
		      i++;
		    }
		    // compare first non-equal ordinal number
		    if (i < vals1.length && i < vals2.length) {
		        int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
		        return Integer.signum(diff);
		    }
		    // the strings are equal or one string is a substring of the other
		    // e.g. "1.2.3" = "1.2.3" or "1.2.3" < "1.2.3.4"
		    return Integer.signum(vals1.length - vals2.length);
		}).reduce((first, second) -> second);
		
		if(optionalVersion.isPresent()) {
			VersionModel version = optionalVersion.get();
			
			document.put("version", buildVersion(version));

			index("docsearch", "project", project.getId().toString(), document);
		}
		
		
	}
	
	private Map<String, Object> buildVersion(VersionModel version) {
		Map<String, Object> document = new HashMap<>();

		document.put("code", version.getCode());
		document.put("url", versionUrlResolver.resolve(version));
		
		return document;
	}

	
	private Map<String, Object> buildProject(ProjectModel project) {
		Map<String, Object> document = new HashMap<>();

		document.put("artifactId", project.getArtifactId());
		document.put("groupId", project.getGroupId());
		document.put("url", projectUrlResolver.resolve(project));
		
		return document;
	}
	
	private Map<String, Object> buildMethod(MethodModel method) {
		Map<String, Object> document = new HashMap<>();

		document.put("name", method.getName());
		document.put("signature", method.getSignature());
		document.put("documentation", method.getDocumentation());
		
		return document;
	}
	
	private void index(String index, String type, String id, Map<String, Object> document) {
		IndexResponse response = client.prepareIndex(index, type, id)
			.setSource(document)
			.execute()
			.actionGet();
		
		if(response.getShardInfo().getFailed() != 0) {
			LOG.error("could not index document of type {} in index {} with id {}.", type, index, id);
		}
	}

	public void setProjectUrlResolver(UrlResolver<ProjectModel> projectUrlResolver) {
		this.projectUrlResolver = projectUrlResolver;
	}

	public void setVersionUrlResolver(UrlResolver<VersionModel> versionUrlResolver) {
		this.versionUrlResolver = versionUrlResolver;
	}

	public void setPackageUrlResolver(UrlResolver<PackageModel> packageUrlResolver) {
		this.packageUrlResolver = packageUrlResolver;
	}

	public void setClassUrlResolver(UrlResolver<ClassModel> classUrlResolver) {
		this.classUrlResolver = classUrlResolver;
	}

	public void setClient(Client client) {
		this.client = client;
	}
	
	
}
