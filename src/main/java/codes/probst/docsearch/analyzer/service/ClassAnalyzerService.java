package codes.probst.docsearch.analyzer.service;

/**
 * An analyzer for class {@code byte}s.
 * @author Benjamin Probst
 *
 */
public interface ClassAnalyzerService {

	/**
	 * Retrieves the class name fron the class {@code byte}s.
	 * @param bytes The class bytes
	 * @return Class name
	 */
	public abstract String getClassNameFromBytes(byte[] bytes);
	
	/**
	 * Analyzes the class {@code byte}s.
	 * @param bytes The class bytes
	 * @return The analysis result
	 */
	public abstract ClassAnalysis analyzeClass(byte[] bytes);

}