package codes.probst.docsearch.analyzer.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class MethodUsageMethodVisitor extends MethodVisitor {
	private static final Pattern DESCRIPTION_PATTERN = Pattern.compile("\\(([^\\)]*)\\)(.*)");
	
	private String className;
	private int lineNumber;
	private Collection<MethodUsageAnalysis> methodUsageAnalysises;
	
	public MethodUsageMethodVisitor() {
		super(Opcodes.ASM5);
		
		methodUsageAnalysises = new ArrayList<>();
	}
	
	public void visitMethodInsn(int opcode, String owner, String name, String desc) {
		handleMethod(owner, name, desc);
	}
	
	@Override
	public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
		handleMethod(owner, name, desc);
	}
	
	@Override
	public void visitLineNumber(int line, Label start) {
		lineNumber = line;
	}
	
	private void handleMethod(String owner, String name, String desc) {
		if("<init>".equals(name)) {
			return;
		}
		
		Matcher m = DESCRIPTION_PATTERN.matcher(desc);
		
		List<String> parameters = new ArrayList<>();
		String returnType = null;
		if(m.find()) {
			String parameterRaw = m.group(1);
			String returnRaw = m.group(2);

			if(!parameterRaw.isEmpty()) {
				parameters.addAll(DescUtil.getClassesFromDescription(parameterRaw));
			}
			
			returnType = DescUtil.getClassesFromDescription(returnRaw).get(0);
		}

		StringBuilder signatureBuilder = new StringBuilder(name);
		signatureBuilder.append("(");
		signatureBuilder.append(parameters.stream().collect(Collectors.joining(", ")));
		signatureBuilder.append(")");
		methodUsageAnalysises.add(new MethodUsageAnalysis(className, ClassUtil.toClassName(owner), name, parameters, signatureBuilder.toString(), returnType, lineNumber));
	}
	
	public Collection<MethodUsageAnalysis> getMethodUsageAnalysises() {
		return methodUsageAnalysises;
	}
	
	public void setClassName(String className) {
		this.className = className;
	}
}
