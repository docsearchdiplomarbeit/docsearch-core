package codes.probst.docsearch.analyzer.service;

import java.util.Collection;

import codes.probst.docsearch.analyzer.service.MethodAnalysis;

/**
 * Holds all data about a class.
 * @author Benjamin Probst
 *
 */
public class ClassAnalysis {
	private String className;
	private Collection<MethodAnalysis> methodAnalysis;
	private Collection<ConstructorAnalysis> constructorAnalysis;
	private Collection<MethodUsageAnalysis> methodUsageAnalysises;
	
	public ClassAnalysis(String className, Collection<MethodAnalysis> methodAnalysis, Collection<ConstructorAnalysis> constructorAnalysis, Collection<MethodUsageAnalysis> methodUsageAnalysises) {
		this.className = className;
		this.methodAnalysis = methodAnalysis;
		this.constructorAnalysis = constructorAnalysis;
		this.methodUsageAnalysises = methodUsageAnalysises;
	}

	/**
	 * Returns all method analysises.
	 * @return Method analysises
	 */
	public Collection<MethodAnalysis> getMethodAnalysis() {
		return methodAnalysis;
	}
	
	/**
	 * Returns all constructor analysises.
	 * @return Constructor analysises
	 */
	public Collection<ConstructorAnalysis> getConstructorAnalysis() {
		return constructorAnalysis;
	}
	
	/**
	 * Returns the name of the analyzed class.
	 * @return Class name
	 */
	public String getClassName() {
		return className;
	}
	
	public Collection<MethodUsageAnalysis> getMethodUsageAnalysises() {
		return methodUsageAnalysises;
	}
	
}
