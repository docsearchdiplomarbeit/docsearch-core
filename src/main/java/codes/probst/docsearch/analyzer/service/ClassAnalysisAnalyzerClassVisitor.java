package codes.probst.docsearch.analyzer.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * Extracts all constructors, methods and the class name of a analyzed class.
 * @author Benjamin Probst
 *
 */
public class ClassAnalysisAnalyzerClassVisitor extends ClassVisitor {
	private static final Pattern DESCRIPTION_PATTERN = Pattern.compile("\\(([^\\)]*)\\)(.*)");
	
	private String className;
	private List<MethodAnalysis> methods;
	private List<ConstructorAnalysis> constructors;
	private MethodUsageMethodVisitor methodVisitor;

	public ClassAnalysisAnalyzerClassVisitor() {
		super(Opcodes.ASM5);
		
		methods = new ArrayList<>();
		constructors = new ArrayList<>();
		methodVisitor = new MethodUsageMethodVisitor();
	}

	/**
	 * Reads the class name.
	 */
	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
		className = ClassUtil.toClassName(name);
		methodVisitor.setClassName(className);
	}
	
	/**
	 * Reads the methods and constructors.
	 */
	@Override
	public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
		Matcher m = DESCRIPTION_PATTERN.matcher(desc);
		
		List<String> parameters = new ArrayList<>();
		String returnType = null;
		if(m.find()) {
			String parameterRaw = m.group(1);
			String returnRaw = m.group(2);

			if(!parameterRaw.isEmpty()) {
				parameters.addAll(DescUtil.getClassesFromDescription(parameterRaw));
			}
			
			returnType = DescUtil.getClassesFromDescription(returnRaw).get(0);
		}
		if (name.equals("<init>")) {
			constructors.add(new ConstructorAnalysis(access, parameters));
		} else if(name.equals("<clinit>")){
			//we do not handle static initialization code
		} else {
			StringBuilder signatureBuilder = new StringBuilder(name);
			signatureBuilder.append("(");
			signatureBuilder.append(parameters.stream().collect(Collectors.joining(", ")));
			signatureBuilder.append(")");
			methods.add(new MethodAnalysis(access, name, signatureBuilder.toString(), returnType, parameters));
		}
		
		return methodVisitor;
	}

	/**
	 * Returns the class name.
	 * @return Class name
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * Returns the analyzed methods.
	 * @return Analyzed methods
	 */
	public Collection<MethodAnalysis> getMethods() {
		return methods;
	}
	
	/**
	 * Returns the analyzed constructors.
	 * @return Analyzed constructors
	 */
	public List<ConstructorAnalysis> getConstructors() {
		return constructors;
	}
	
	public Collection<MethodUsageAnalysis> getMethodUsageAnalysises() {
		return methodVisitor.getMethodUsageAnalysises();
	}
}
