package codes.probst.docsearch.analyzer.service;

import java.io.File;

/**
 * Utility class for working with ASM.
 * @author Benjamin Probst
 *
 */
public class ClassUtil {

	/**
	 * Maps a file path of a java class to its full qualified class name.
	 * @param Name the file path
	 * @return Full qualified class name
	 */
	public static String toClassName(String name) {
		return name.replace("/", ".").replace("\\", ".");
	}
	
	/**
	 * Maps a full qualified class name to a file path.
	 * @param name Full qualified class name
	 * @return File path
	 */
	public static String toFileName(String name) {
		return name.replace(".", File.separator);
	}

	/**
	 * Extracts the package from a full qualified class name.
	 * @param name The full qualified class name
	 * @return The package
	 */
	public static String extractPackage(String name) {
		int index = name.lastIndexOf(".");
		return index == -1 ? "" : name.substring(0, index);
	}

	/**
	 * Extracts the class name from a full qualified class name.
	 * @param name The full qualified class name
	 * @return The class name
	 */
	public static String extractClassName(String name) {
		int index = name.lastIndexOf(".");
		return index == -1 ? name : name.substring(index + 1);
	}
	
	/**
	 * Cleans the class name of all additional information.
	 * @param name The class name
	 * @return The cleaned class name
	 */
	public static String cleanClassName(String name) {
		return name.replace("[]", "");
	}
	
}
