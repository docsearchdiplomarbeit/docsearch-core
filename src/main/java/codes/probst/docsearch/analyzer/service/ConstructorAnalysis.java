package codes.probst.docsearch.analyzer.service;

import java.util.List;

/**
 * Holds all data about a analyzed constructor.
 * @author Benjamin Probst
 *
 */
public class ConstructorAnalysis {
	private int access;
	private List<String> parameters;

	public ConstructorAnalysis(int access, List<String> parameters) {
		this.access = access;
		this.parameters = parameters;
	}

	/**
	 * Returns the access level of a constructor.
	 * @return access level
	 */
	public int getAccess() {
		return access;
	}

	/**
	 * Returns all parameters used by this constructor.
	 * @return All parameters
	 */
	public List<String> getParameters() {
		return parameters;
	}

}
