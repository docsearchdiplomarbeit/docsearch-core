package codes.probst.docsearch.analyzer.projectinformation;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codes.probst.framework.file.service.FileService;

/**
 * Default implementation of the {@code ProjectInformationAnalyzer} interface that is based on a {@code Collection} of 
 * {@code ProjectInformationEvaluator} for different {@code ProjectType}s.
 * This implementation requires the project to be provided as a zip file.
 * @author Benjamin Probst
 *
 */
public class DefaultProjectInformationAnalyzer implements ProjectInformationAnalyzer {
	private static final Logger LOG = LoggerFactory.getLogger(DefaultProjectInformationAnalyzer.class);
	
	private Collection<ProjectInformationEvaluator> evaluators;
	private FileService fileService;
	
	/**
	 * Analyzes the given data to retrieve the {@code ProjectInformation} and assumes that the given {@code byte}s of data 
	 * represents a zip file.
	 * @param data The data of a zip file
	 * @return The information of a project if available
	 */
	@Override
	public Optional<ProjectInformation> analyze(byte[] data) {
		if(evaluators == null || evaluators.isEmpty()) {
			LOG.warn("Could not find ProjectInformation because there is no Evaluator.");
			return Optional.empty();
		}
		
		ProjectInformation information = null;
		
		try(
			ByteArrayInputStream bais = new ByteArrayInputStream(data);
			ZipInputStream is = new ZipInputStream(bais);
		) {
			ENTRY_LOOP:
			for(ZipEntry zipEntry;null != (zipEntry = is.getNextEntry());) {
				byte[] entryData = null;
				for(ProjectInformationEvaluator evaluator : evaluators) {
					if(evaluator.canHandleEntry(zipEntry)) {
						if(entryData == null) {
							entryData = fileService.getBytesFromInputStream(is);
						}
						
						information = evaluator.evaluate(entryData);
						if(isValidProjectInformation(information)) {
							break ENTRY_LOOP;
						} else {
							information = null;
						}
					}
				}
			}
		} catch(IOException e) {
			LOG.error("Exception while searching for ProjectInformation.", e);
		}
		
		if(information == null) {
			LOG.info("Could not find ProjectInformation");
		} else {
			LOG.info("Found ProjectInformation[groupId: {}, artifactId: {}, version: {}, type: {}]",
				information.getGroupId(),
				information.getArtifactId(),
				information.getVersion(),
				information.getType()
			);
		}
		
		return Optional.ofNullable(information);
	}
	
	/**
	 * Validates if a given {@code ProjectInformation} contains all required information.
	 * @param information The information to be validated
	 * @return {@code true} if all required information is present and {@code false} otherwise
	 */
	protected boolean isValidProjectInformation(ProjectInformation information) {
		return information != null
			&& information.getGroupId() != null
			&& !information.getGroupId().isEmpty()
			&& information.getArtifactId() != null
			&& !information.getArtifactId().isEmpty()
			&& information.getVersion() != null
			&& !information.getVersion().isEmpty()
			&& information.getType() != null;
	}
	
	/**
	 * Sets all {@code ProjectInformationEvaluator}s that should be used by this {@code ProjectInformationAnalyzer}.
	 * @param evaluators All evaluators
	 */
	public void setEvaluators(Collection<ProjectInformationEvaluator> evaluators) {
		this.evaluators = evaluators;
	}
	
	/**
	 * Sets the file service to retrieve bytes.
	 * @param fileService The service
	 */
	public void setFileService(FileService fileService) {
		this.fileService = fileService;
	}
	
}
