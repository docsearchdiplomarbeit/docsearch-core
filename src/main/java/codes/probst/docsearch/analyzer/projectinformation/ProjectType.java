package codes.probst.docsearch.analyzer.projectinformation;

/**
 * Represents all available and supported project types supported.
 * @author Benjamin Probst
 *
 */
public enum ProjectType {
	JAVA, MAVEN, GRADLE, PLAIN_JAVA;
}
