package codes.probst.docsearch.analyzer.projectinformation;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Evaluates Java Runtime Environment projects and retrieves its version.
 * The groupId is fixed as java and the artifactId as jdk.
 * @author Benjamin Probst
 *
 */
public class JavaProjectInformationEvaluator implements ProjectInformationEvaluator {
	private static final Logger LOG = LoggerFactory.getLogger(JavaProjectInformationEvaluator.class);
	private static final Pattern VERSION_PATTERN = Pattern.compile("([0-9]+\\.[0-9]+).*");
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ProjectInformation evaluate(byte[] bytes) {
		ProjectInformation information = null;
		
		try(ByteArrayInputStream bais = new ByteArrayInputStream(bytes)) {
			Manifest manifest = new Manifest(bais);
			
			Attributes attributes = manifest.getMainAttributes();
			if("Java Runtime Environment".equals(attributes.getValue("Implementation-Title"))) {
				String groupId = "java";
				String artifactId = "jdk";
				String version = attributes.getValue("Implementation-Version");
				if(version != null) {
					//only handle the first 2 version informations of a version (e.g 1.2.4_23 -> 1.2)
					Matcher matcher = VERSION_PATTERN.matcher(version);
					if(matcher.find()) {
						version = matcher.group(1);
					}
				}
				
				information = new ProjectInformation(ProjectType.JAVA, groupId, artifactId, version);
			}
		} catch (IOException e) {
			LOG.error("Could not evaluate java information from Manifest", e);
		}
		
		return information;
	}
	
	/**
	 * Validates if the given {@code ZipEntry} is the MANIFEST.MF file in the META-INF folder.
	 * @param entry the entry to check
	 * @return {@code true} if its the MANIFEST.MF file and {@code false} otherwise
	 */
	@Override
	public boolean canHandleEntry(ZipEntry entry) {
		return "META-INF/MANIFEST.MF".equals(entry.getName());
	}
}
