package codes.probst.docsearch.clazz.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CLASS_USAGE")
@SequenceGenerator(initialValue = 0, name = "ClassUsageSequence", sequenceName="CLASS_USAGE_SEQ")
public class ClassUsageModel {
	@Id
	@GeneratedValue(generator = "ClassUsageSequence", strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "classId", nullable = false)
	private ClassModel clazz;
	@Column(name = "array")
	private boolean array;
	@Column(name = "documentation", length = 65535)
	private String documentation;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public ClassModel getClazz() {
		return clazz;
	}
	
	public void setClazz(ClassModel clazz) {
		this.clazz = clazz;
	}
	
	public void setArray(boolean array) {
		this.array = array;
	}
	
	public boolean isArray() {
		return array;
	}
	
	public void setDocumentation(String documentation) {
		this.documentation = documentation;
	}
	
	public String getDocumentation() {
		return documentation;
	}
}
