package codes.probst.docsearch.clazz.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.docsearch.packages.model.PackageModel;

/**
 * Representation of a {@code Class} for this application that contains the compiled and source bytes
 * and also the documentation if available.
 * @author Benjamin Probst
 *
 */
@Entity
@Table(name = "CLASS")
@SequenceGenerator(initialValue = 0, name = "ClassSequence", sequenceName="CLASS_SEQ")
public class ClassModel {
	@Id
	@GeneratedValue(generator = "ClassSequence", strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	@Column(name = "name", nullable = false)
	private String name;
	@Lob
	@Column(name = "compilation", length = Integer.MAX_VALUE)
	private byte[] compilation;
	@Lob
	@Column(name = "source", length = Integer.MAX_VALUE)
	private byte[] source;
	@Column(name = "documentation", length = Integer.MAX_VALUE)
	private String documentation;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "packageId", nullable = false)
	private PackageModel packageModel;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "clazz")
	private List<MethodModel> methods;

	/**
	 * Returns the id.
	 * @return The id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 * @param id The id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Returns the name.
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * @param name The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the compiled class bytes.
	 * @return The compiled class bytes
	 */
	public byte[] getCompilation() {
		return compilation;
	}

	/**
	 * Sets the compiled class bytes.
	 * @param compilation The compiled class bytes
	 */
	public void setCompilation(byte[] compilation) {
		this.compilation = compilation;
	}

	/**
	 * Sets the source bytes of a class.
	 * @return The source bytes
	 */
	public byte[] getSource() {
		return source;
	}

	/**
	 * Sets the source bytes of a class.
	 * @param source The source bytes
	 */
	public void setSource(byte[] source) {
		this.source = source;
	}

	/**
	 * Returns the documentation of a class.
	 * @return The documentation
	 */
	public String getDocumentation() {
		return documentation;
	}

	/**
	 * Sets the documentation of a class.
	 * @param documentation The documentation
	 */
	public void setDocumentation(String documentation) {
		this.documentation = documentation;
	}

	/**
	 * Returns the {@code PackageModel} that this class is contained in.
	 * @return The package model
	 */
	public PackageModel getPackageModel() {
		return packageModel;
	}

	/**
	 * Sets the {@code PackageModel} that contains this class.
	 * @param packageModel The package model
	 */
	public void setPackageModel(PackageModel packageModel) {
		this.packageModel = packageModel;
	}

	/**
	 * Returns all methods contained by this class.
	 * @return All methods
	 */
	public List<MethodModel> getMethods() {
		return methods;
	}

	/**
	 * Sets all methods contained by this class.
	 * @param methods All methods
	 */
	public void setMethods(List<MethodModel> methods) {
		this.methods = methods;
	}

}
