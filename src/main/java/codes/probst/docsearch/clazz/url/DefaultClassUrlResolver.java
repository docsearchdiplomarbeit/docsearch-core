package codes.probst.docsearch.clazz.url;

import java.util.HashMap;
import java.util.Map;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.url.AbstractUrlResolver;

public class DefaultClassUrlResolver extends AbstractUrlResolver<ClassModel> {
	
	@Override
	protected Map<String, String> getPathParameters(ClassModel value) {
		Map<String, String> parameters = new HashMap<>();
		
		PackageModel packagez = value.getPackageModel();
		if(packagez != null) {
			parameters.put("package-id", packagez.getId().toString());
			parameters.put("package-name", packagez.getName());
			
			VersionModel version = packagez.getVersion();
			
			if(version != null) {
				parameters.put("version-id", version.getId().toString());
				parameters.put("version-code", version.getCode());
				
				ProjectModel project = version.getProject();
				if(project != null) {
					parameters.put("project-id", project.getId().toString());
					parameters.put("project-artifact-id", project.getArtifactId());
					parameters.put("project-group-id", project.getGroupId());
				}
			}
		}
		
		parameters.put("class-name", value.getName().replace("$", "."));
		parameters.put("class-id", value.getId().toString());
		
		return parameters;
	}
	
	@Override
	protected String getPattern() {
		return "/projects/{project-artifact-id}/{project-id}/{version-code}/{version-id}/{package-name}/{package-id}/{class-name}/{class-id}";
	}
}
