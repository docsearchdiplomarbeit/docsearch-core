package codes.probst.docsearch.clazz.dao;

import codes.probst.docsearch.clazz.model.ClassUsageModel;
import codes.probst.docsearch.clazz.model.ClassUsageModel_;
import codes.probst.framework.dao.AbstractJpaDao;

public class JpaClassUsageDao extends AbstractJpaDao<ClassUsageModel, Long> implements ClassUsageDao {

	public JpaClassUsageDao() {
		super(ClassUsageModel.class, ClassUsageModel_.id);
	}
}
