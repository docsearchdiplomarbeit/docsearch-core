package codes.probst.docsearch.clazz.dao;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.model.ClassModel_;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.packages.model.PackageModel_;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.docsearch.version.model.VersionModel_;
import codes.probst.framework.dao.AbstractJpaDao;
import codes.probst.framework.pagination.Pageable;

public class JpaClassDao extends AbstractJpaDao<ClassModel, Long> implements ClassDao {

	public JpaClassDao() {
		super(ClassModel.class, ClassModel_.id);
	}
	
	@Override
	public Collection<ClassModel> findByPackageId(Long packageId, Pageable pageable) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<ClassModel> criteria = builder.createQuery(ClassModel.class);
		Root<ClassModel> root = criteria.from(ClassModel.class);
		criteria.where(builder.equal(root.get(ClassModel_.packageModel), packageId));
		criteria.select(root);
		return select(criteria, pageable);
	}

	@Override
	public Optional<ClassModel> findByNameAndPackageInVersions(String className, String packageName, List<Long> versionIds) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<ClassModel> criteria = builder.createQuery(ClassModel.class);
		Root<ClassModel> root = criteria.from(ClassModel.class);
		Join<ClassModel, PackageModel> packageJoin = root.join(ClassModel_.packageModel);
		Join<PackageModel, VersionModel> versionJoin = packageJoin.join(PackageModel_.version);
		
		criteria.where(
			builder.and(
				builder.equal(root.get(ClassModel_.name), className),
				builder.equal(packageJoin.get(PackageModel_.name), packageName),
				versionJoin.get(VersionModel_.id).in(versionIds)
			)
		);
		return selectOnce(criteria);
	}

}
