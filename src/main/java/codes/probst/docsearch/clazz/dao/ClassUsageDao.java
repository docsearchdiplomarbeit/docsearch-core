package codes.probst.docsearch.clazz.dao;

import codes.probst.docsearch.clazz.model.ClassUsageModel;
import codes.probst.framework.dao.Dao;

public interface ClassUsageDao extends Dao<ClassUsageModel, Long> {

}