package codes.probst.docsearch.clazz.facade;

import java.util.Collection;

import codes.probst.docsearch.method.facade.MethodData;
import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.project.facade.ProjectData;
import codes.probst.docsearch.version.facade.VersionData;

public class ClassData {
	private Long id;
	private String name;
	private String url;
	private PackageData packagez;
	private VersionData version;
	private ProjectData project;
	private String description;
	private String source;
	private Collection<MethodData> methods;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public VersionData getVersion() {
		return version;
	}

	public void setVersion(VersionData version) {
		this.version = version;
	}

	public ProjectData getProject() {
		return project;
	}

	public void setProject(ProjectData project) {
		this.project = project;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
	public PackageData getPackagez() {
		return packagez;
	}
	
	public void setPackagez(PackageData packagez) {
		this.packagez = packagez;
	}
	
	public Collection<MethodData> getMethods() {
		return methods;
	}
	
	public void setMethods(Collection<MethodData> methods) {
		this.methods = methods;
	}
}
