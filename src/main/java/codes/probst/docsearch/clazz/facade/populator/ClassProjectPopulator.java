package codes.probst.docsearch.clazz.facade.populator;

import java.util.Optional;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.facade.ClassPopulationOption;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.project.facade.ProjectData;
import codes.probst.docsearch.project.facade.ProjectFacade;
import codes.probst.docsearch.project.facade.ProjectPopulationOption;
import codes.probst.framework.bean.BeanResolver;
import codes.probst.framework.populator.Populator;

public class ClassProjectPopulator implements Populator<ClassModel, ClassData> {
	private BeanResolver beanResolver;
	private ProjectFacade projectFacade;
	
	@Override
	public void populate(ClassModel source, ClassData target) {
		Optional<ProjectData> optional = getProjectFacade().getByClassId(source.getId(), ProjectPopulationOption.OPTION_URL);
		target.setProject(optional.isPresent() ? optional.get() : null);
		
	}

	private ProjectFacade getProjectFacade() {
		if(projectFacade == null) {
			projectFacade = beanResolver.resolve(ProjectFacade.class);
		}
		return projectFacade;
	}
	
	public void setBeanResolver(BeanResolver beanResolver) {
		this.beanResolver = beanResolver;
	}
	
}
