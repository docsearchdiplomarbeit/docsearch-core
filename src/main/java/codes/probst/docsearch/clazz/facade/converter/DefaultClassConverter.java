package codes.probst.docsearch.clazz.facade.converter;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.framework.converter.Converter;

public class DefaultClassConverter implements Converter<ClassModel, ClassData> {

	@Override
	public ClassData convert(ClassModel value) {
		ClassData data = new ClassData();
		
		data.setId(value.getId());
		data.setName(value.getName() == null ? null : value.getName().replace("$", "."));
		
		return data;
	}
}
