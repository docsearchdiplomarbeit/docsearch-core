package codes.probst.docsearch.clazz.facade;

import codes.probst.framework.populator.PopulationOption;

public class ClassPopulationOption extends PopulationOption {
	public static final ClassPopulationOption OPTION_VERSION = new ClassPopulationOption("VERSION", 1);
	public static final ClassPopulationOption OPTION_PROJECT = new ClassPopulationOption("PROJECT", 1);
	public static final ClassPopulationOption OPTION_PACKAGE = new ClassPopulationOption("PACKAGE", 1);
	public static final ClassPopulationOption OPTION_URL = new ClassPopulationOption("URL", 1);
	public static final ClassPopulationOption OPTION_SHORT_DESCRIPTION = new ClassPopulationOption("SHORT_DESCRIPTION", 1);
	public static final ClassPopulationOption OPTION_DESCRIPTION = new ClassPopulationOption("DESCRIPTION", 1);
	public static final ClassPopulationOption OPTION_METHODS = new ClassPopulationOption("METHODS", 1);
	public static final ClassPopulationOption OPTION_SOURCE = new ClassPopulationOption("SOURCE", 1);
	
	protected ClassPopulationOption(String code, int order) {
		super("CLASS_POPULATION_OPTION_" + code, order);
	}
	
}
