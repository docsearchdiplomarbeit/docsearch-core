package codes.probst.docsearch.clazz.facade;

import java.util.Collection;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.framework.facade.Facade;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.populator.PopulationOption;

public interface ClassFacade extends Facade<ClassModel, ClassData, Long> {

	public Collection<ClassData> getByPackageId(Long packageId, Pageable pageable, PopulationOption... options);

}	