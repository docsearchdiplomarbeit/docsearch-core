package codes.probst.docsearch.method.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.model.ClassUsageModel;

/**
 * Representation of a {@code Method} for this application.
 * @author Benjamin Probst
 *
 */
@Entity
@Table(name = "METHOD")
@SequenceGenerator(initialValue = 0, name = "MethodSequence", sequenceName="METHOD_SEQ")
public class MethodModel  {
	@Id
	@GeneratedValue(generator = "MethodSequence", strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	@Column(name = "name", nullable = false)
	private String name;
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "METHOD_PARAMETER", joinColumns = {
			@JoinColumn(name = "methodId", nullable = false, updatable = false)
		}, inverseJoinColumns = {
				@JoinColumn(name = "classUsageId", nullable = false, updatable = false)
		}
	)
	private Collection<ClassUsageModel> parameters;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "returnTypeId", nullable = false)
	private ClassUsageModel returnType;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "classId", nullable = false)
	private ClassModel clazz;
	@Column(name = "documentation", length = Integer.MAX_VALUE)
	private String documentation;;
	@Column(name = "signature", nullable = false, length = Integer.MAX_VALUE)
	private String signature;
	
	/**
	 * Returns the id.
	 * @return The id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 * @param id The id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Returns the name of the method.
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the method.
	 * @param name The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns all parameters of this method.
	 * @return All parameters
	 */
	public Collection<ClassUsageModel> getParameters() {
		return parameters;
	}

	/**
	 * Sets all parameters of this method.
	 * @param parameters All parameters
	 */
	public void setParameters(Collection<ClassUsageModel> parameters) {
		this.parameters = parameters;
	}

	/**
	 * Returns the return type of this method.
	 * @return The return type
	 */
	public ClassUsageModel getReturnType() {
		return returnType;
	}

	/**
	 * Sets the return type of this method.
	 * @param returnType The return type
	 */
	public void setReturnType(ClassUsageModel returnType) {
		this.returnType = returnType;
	}

	public ClassModel getClazz() {
		return clazz;
	}
	
	public void setClazz(ClassModel clazz) {
		this.clazz = clazz;
	}
	
	public void setDocumentation(String documentation) {
		this.documentation = documentation;
	}
	
	public String getDocumentation() {
		return documentation;
	}
	
	public void setSignature(String signature) {
		this.signature = signature;
	}
	
	public String getSignature() {
		return signature;
	}
}
