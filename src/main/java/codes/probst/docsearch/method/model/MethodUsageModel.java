package codes.probst.docsearch.method.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import codes.probst.docsearch.clazz.model.ClassModel;

@Entity
@Table(name = "METHOD_USAGE")
@SequenceGenerator(initialValue = 0, name = "MethodUsageSequence", sequenceName = "METHOD_USAGE_SEQ")
public class MethodUsageModel {
	@Id
	@GeneratedValue(generator = "MethodUsageSequence", strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "methodId", nullable = false)
	private MethodModel method;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "callingClassId", nullable = false)
	private ClassModel callingClass;
	@Column(name = "lineNumber", nullable = false)
	private int lineNumber;
	@Column(name = "snipped")
	private String snipped;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MethodModel getMethod() {
		return method;
	}

	public void setMethod(MethodModel method) {
		this.method = method;
	}

	public ClassModel getCallingClass() {
		return callingClass;
	}

	public void setCallingClass(ClassModel callingClass) {
		this.callingClass = callingClass;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getSnipped() {
		return snipped;
	}

	public void setSnipped(String snipped) {
		this.snipped = snipped;
	}

}
