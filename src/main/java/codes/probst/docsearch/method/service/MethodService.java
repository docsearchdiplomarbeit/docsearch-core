package codes.probst.docsearch.method.service;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.docsearch.method.model.MethodUsageModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.service.Service;

public interface MethodService extends Service<MethodModel, Long> {

	public Collection<MethodModel> getByClassId(Long classId, Pageable pageable);

	public void save(MethodUsageModel methodUsage);

	public Optional<MethodModel> getByClassIdAndNameAndReturnTypeIdAndSignature(Long classId, String name, Long returnTypeId, String signature);
	
	public Collection<MethodUsageModel> getUsageByMethodId(Long methodId, Pageable pageable);
	
}