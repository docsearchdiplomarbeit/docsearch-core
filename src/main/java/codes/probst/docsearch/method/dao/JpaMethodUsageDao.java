package codes.probst.docsearch.method.dao;

import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import codes.probst.docsearch.method.model.MethodUsageModel;
import codes.probst.docsearch.method.model.MethodUsageModel_;
import codes.probst.framework.dao.AbstractJpaDao;
import codes.probst.framework.pagination.Pageable;

public class JpaMethodUsageDao extends AbstractJpaDao<MethodUsageModel, Long> implements MethodUsageDao {

	public JpaMethodUsageDao() {
		super(MethodUsageModel.class, null);
	}
	
	@Override
	public Collection<MethodUsageModel> findByMethodId(Long methodId, Pageable pageable) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<MethodUsageModel> criteria = builder.createQuery(MethodUsageModel.class);
		Root<MethodUsageModel> root = criteria.from(MethodUsageModel.class);
		root.join(MethodUsageModel_.method);
		criteria.where(
			builder.and(
				builder.equal(root.get(MethodUsageModel_.method), methodId),
				builder.isNotNull(root.get(MethodUsageModel_.snipped)),
				builder.notEqual(root.get(MethodUsageModel_.snipped), "")
			)
		);
		
		return select(criteria, pageable);
	}

}
