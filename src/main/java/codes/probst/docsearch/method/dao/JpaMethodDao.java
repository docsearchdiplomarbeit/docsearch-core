package codes.probst.docsearch.method.dao;

import java.util.Collection;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.docsearch.method.model.MethodModel_;
import codes.probst.framework.dao.AbstractJpaDao;
import codes.probst.framework.pagination.Pageable;

public class JpaMethodDao extends AbstractJpaDao<MethodModel, Long> implements MethodDao {

	public JpaMethodDao() {
		super(MethodModel.class, MethodModel_.id);
	}

	@Override
	public Collection<MethodModel> findByClassId(Long classId, Pageable pageable) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<MethodModel> criteria = builder.createQuery(MethodModel.class);
		Root<MethodModel> root = criteria.from(MethodModel.class);
		criteria.where(builder.equal(root.get(MethodModel_.clazz), classId));
		return select(criteria, pageable);
	}
	
	@Override
	public Optional<MethodModel> findByClassIdAndNameAndReturnTypeIdAndSignature(Long classId, String name, Long returnTypeId, String signature) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<MethodModel> criteria = builder.createQuery(MethodModel.class);
		Root<MethodModel> root = criteria.from(MethodModel.class);
		criteria.where(
			builder.and(
				builder.equal(root.get(MethodModel_.clazz), classId),
				builder.equal(root.get(MethodModel_.name), name),
				builder.equal(root.get(MethodModel_.returnType), returnTypeId),
				builder.equal(root.get(MethodModel_.signature), signature)
			)
		);
		return selectOnce(criteria);
	}
}
