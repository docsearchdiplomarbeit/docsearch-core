package codes.probst.docsearch.method.facade.converter;

import codes.probst.docsearch.method.facade.MethodReferenceData;
import codes.probst.docsearch.method.model.MethodUsageModel;
import codes.probst.framework.converter.Converter;

public class DefaultMethodUsageConverter implements Converter<MethodUsageModel, MethodReferenceData> {
	public MethodReferenceData convert(MethodUsageModel value) {
		MethodReferenceData data = new MethodReferenceData();
		
		data.setSnipped(value.getSnipped());
		
		return data;
	}
}
