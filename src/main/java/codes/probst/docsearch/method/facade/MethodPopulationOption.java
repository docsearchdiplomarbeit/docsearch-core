package codes.probst.docsearch.method.facade;

import codes.probst.framework.populator.PopulationOption;

public class MethodPopulationOption extends PopulationOption{
	public static final MethodPopulationOption OPTION_REFERENCES = new MethodPopulationOption("REFERENCES", 1);
	public static final MethodPopulationOption OPTION_TAGS = new MethodPopulationOption("TAGS", 1);
	
	
	protected MethodPopulationOption(String code, int order) {
		super("METHOD_POPULATION_OPTION_" + code, order);
	}

	
}
