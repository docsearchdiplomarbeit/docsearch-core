package codes.probst.docsearch.method.facade.converter;

import codes.probst.docsearch.method.facade.MethodData;
import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.framework.converter.Converter;

public class DefaultMethodConverter implements Converter<MethodModel, MethodData> {
	
	@Override
	public MethodData convert(MethodModel value) {
		MethodData data = new MethodData();
		
		data.setDocumentation(value.getDocumentation());
		data.setName(value.getName());
		data.setSignature(value.getReturnType().getClazz().getName() + " " + value.getSignature());
		
		return data;
	}
}
