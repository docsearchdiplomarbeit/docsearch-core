package codes.probst.docsearch.method.facade;

import java.util.Collection;

import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.docsearch.method.service.MethodService;
import codes.probst.framework.facade.AbstractFacade;
import codes.probst.framework.pagination.Pageable;

public class DefaultMethodFacade extends AbstractFacade<MethodModel, MethodData, MethodService, Long> implements MethodFacade {

	
	@Override
	public Collection<MethodData> getByClassId(Long classId, Pageable pageable, MethodPopulationOption... options) {
		return convertAndPopulateAll(getService().getByClassId(classId, pageable), options);
	}
	
}
