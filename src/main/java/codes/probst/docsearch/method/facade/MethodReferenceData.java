package codes.probst.docsearch.method.facade;

import codes.probst.docsearch.clazz.facade.ClassData;

public class MethodReferenceData {
	private ClassData clazz;
	private String snipped;
	
	
	public ClassData getClazz() {
		return clazz;
	}
	
	public void setClazz(ClassData clazz) {
		this.clazz = clazz;
	}
	
	public String getSnipped() {
		return snipped;
	}
	
	public void setSnipped(String snipped) {
		this.snipped = snipped;
	}
}
