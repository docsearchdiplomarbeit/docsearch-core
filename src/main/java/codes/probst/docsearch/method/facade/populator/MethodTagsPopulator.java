package codes.probst.docsearch.method.facade.populator;

import java.util.stream.Collectors;

import codes.probst.docsearch.clazz.model.ClassUsageModel;
import codes.probst.docsearch.method.facade.MethodData;
import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.framework.populator.Populator;

public class MethodTagsPopulator implements Populator<MethodModel, MethodData> {

	@Override
	public void populate(MethodModel source, MethodData target) {
		ClassUsageModel returnType = source.getReturnType();
		target.setReturnTypeTag(returnType.getDocumentation());
		if(source.getParameters() != null) {
			target.setParameterTypeTags(source.getParameters().stream().map(p -> p.getDocumentation()).collect(Collectors.toList()));
		}
	}
	
}
