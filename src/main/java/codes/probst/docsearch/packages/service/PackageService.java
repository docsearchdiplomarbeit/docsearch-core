package codes.probst.docsearch.packages.service;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.service.Service;

public interface PackageService extends Service<PackageModel, Long>{

	public Collection<PackageModel> getByVersionId(Long versionId, Pageable pageable);

	public Optional<PackageModel> getByNameAndVersionId(String name, Long versionId);

	public Optional<PackageModel> getByClassId(Long classId);
	
}