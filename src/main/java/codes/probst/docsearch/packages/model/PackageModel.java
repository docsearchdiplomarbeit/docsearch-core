package codes.probst.docsearch.packages.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.version.model.VersionModel;

/**
 * Representation of a {@code Package} for this application.
 * @author Benjamin Probst
 *
 */
@Entity
@Table(name = "PACKAGE")
@SequenceGenerator(initialValue = 0, name = "PackageSequence", sequenceName="PACKAGE_SEQ")
public class PackageModel {
	@Id
	@GeneratedValue(generator = "PackageSequence", strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	@Column(name = "name", nullable = false, length = 500)
	private String name;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "packageModel")
	private List<ClassModel> classes;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "versionId", nullable = false)
	private VersionModel version;
	@Column(name = "documentation", length = 65535)
	private String documentation;

	/**
	 * Returns the id.
	 * @return The id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id
	 * @param id The id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Returns the name of this package.
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of this package.
	 * @param name The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns all classes in this package.
	 * @return All classes
	 */
	public List<ClassModel> getClasses() {
		return classes;
	}

	/**
	 * Sets all classes in this package.
	 * @param classes All classes
	 */
	public void setClasses(List<ClassModel> classes) {
		this.classes = classes;
	}

	/**
	 * Returns the version in which this package is contained.
	 * @return The version
	 */
	public VersionModel getVersion() {
		return version;
	}

	/**
	 * Sets the version in which this package is contained.
	 * @param version The version
	 */
	public void setVersion(VersionModel version) {
		this.version = version;
	}

	public void setDocumentation(String documentation) {
		this.documentation = documentation;
	}
	
	public String getDocumentation() {
		return documentation;
	}
}
