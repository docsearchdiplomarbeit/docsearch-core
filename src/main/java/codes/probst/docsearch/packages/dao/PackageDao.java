package codes.probst.docsearch.packages.dao;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.framework.dao.Dao;
import codes.probst.framework.pagination.Pageable;

public interface PackageDao extends Dao<PackageModel, Long> {

	public Collection<PackageModel> findByVersionId(Long versionId, Pageable pageable);

	public Optional<PackageModel> findByNameAndVersionId(String name, Long versionId);

	public Optional<PackageModel> findByClassId(Long classId);

}