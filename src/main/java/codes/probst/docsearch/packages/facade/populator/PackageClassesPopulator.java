package codes.probst.docsearch.packages.facade.populator;

import codes.probst.docsearch.clazz.facade.ClassFacade;
import codes.probst.docsearch.clazz.facade.ClassPopulationOption;
import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.framework.bean.BeanResolver;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;
import codes.probst.framework.pagination.SortDirection;
import codes.probst.framework.populator.Populator;

public class PackageClassesPopulator implements Populator<PackageModel, PackageData> {
	private BeanResolver beanResolver;
	private ClassFacade classFacade;
	
	@Override
	public void populate(PackageModel source, PackageData target) {
		Pageable pageable = new Pageable();
		pageable.setSort(new Sort[] { new Sort("name", SortDirection.ASCENDING) });
		
		target.setClasses(getClassFacade().getByPackageId(source.getId(), pageable,
			ClassPopulationOption.OPTION_URL,
			ClassPopulationOption.OPTION_SHORT_DESCRIPTION
		));
	}
	
	private ClassFacade getClassFacade() {
		if(classFacade == null) {
			classFacade = beanResolver.resolve(ClassFacade.class);
		}
		return classFacade;
	}
	
	public void setBeanResolver(BeanResolver beanResolver) {
		this.beanResolver = beanResolver;
	}
	
}
