package codes.probst.docsearch.packages.facade;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.framework.facade.Facade;
import codes.probst.framework.pagination.Pageable;

public interface PackageFacade extends Facade<PackageModel, PackageData, Long> {

	public Collection<PackageData> getByVersionId(Long versionId, Pageable pageable, PackagePopulationOption... options);

	public Optional<PackageData> getByClassId(Long classId, PackagePopulationOption... options);

}