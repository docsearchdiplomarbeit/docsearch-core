package codes.probst.docsearch.packages.facade.converter;

import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.framework.converter.Converter;

public class DefaultPackageConverter implements Converter<PackageModel, PackageData> {

	@Override
	public PackageData convert(PackageModel value) {
		PackageData data = new PackageData();
		
		data.setId(value.getId());
		data.setName(value.getName());
		
		return data;
	}
}
