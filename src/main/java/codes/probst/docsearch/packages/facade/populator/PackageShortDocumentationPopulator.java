package codes.probst.docsearch.packages.facade.populator;

import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.util.DocumentationUtil;
import codes.probst.framework.populator.Populator;

public class PackageShortDocumentationPopulator implements Populator<PackageModel, PackageData> {
	private int sentences;
	
	@Override
	public void populate(PackageModel source, PackageData target) {
		String documentation = DocumentationUtil.getDocumentationPart(source.getDocumentation(), sentences);
		target.setDocumentation(documentation);
	}
	
	public void setSentences(int sentences) {
		this.sentences = sentences;
	}
}
