package codes.probst.docsearch.packages.facade;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.version.facade.VersionData;

public class PackageData {
	private Long id;
	private String name;
	private String url;
	private String documentation;
	private VersionData version;
	private Collection<ClassData> classes;
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setDocumentation(String documentation) {
		this.documentation = documentation;
	}
	
	public String getDocumentation() {
		return documentation;
	}

	public void setVersion(VersionData version) {
		this.version = version;
	}
	
	public VersionData getVersion() {
		return version;
	}
	
	public void setClasses(Collection<ClassData> classes) {
		this.classes = classes;
	}
	
	public Collection<ClassData> getClasses() {
		return classes;
	}
	
}
