package codes.probst.docsearch.packages.facade;

import codes.probst.framework.populator.PopulationOption;

public class PackagePopulationOption extends PopulationOption {
	public static final PackagePopulationOption OPTION_URL = new PackagePopulationOption("URL", 1);
	public static final PackagePopulationOption OPTION_SHORT_DOCUMENTATION = new PackagePopulationOption("SHORT_DOCUMENTATION", 1);
	public static final PackagePopulationOption OPTION_DOCUMENTATION = new PackagePopulationOption("DOCUMENTATION", 1);
	public static final PackagePopulationOption OPTION_VERSION = new PackagePopulationOption("VERSION", 1);
	public static final PackagePopulationOption OPTION_CLASSES = new PackagePopulationOption("CLASSES", 1);
	
	protected PackagePopulationOption(String code, int order) {
		super("PACKAGE_POPULATION_OPTION_" + code, order);
	}

	
}
