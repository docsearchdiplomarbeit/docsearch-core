package codes.probst.framework.url;

public class NoopUrlCleaner implements UrlCleaner{

	@Override
	public String clean(String value) {
		return value;
	}

}
