package codes.probst.docsearch.method.facade.converter;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.model.ClassUsageModel;
import codes.probst.docsearch.method.facade.MethodData;
import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.framework.converter.Converter;

public class MethodConverterTest {
	private Converter<MethodModel, MethodData> converter;
	
	@Before
	public void setup() {
		converter = new DefaultMethodConverter();
	}
	
	@Test
	public void testConversion() {
		MethodModel model = new MethodModel();
		model.setId(Long.valueOf(0l));
		model.setDocumentation("Lorem ipsum");
		model.setClazz(new ClassModel());
		model.setName("example");
		model.setParameters(Arrays.asList(new ClassUsageModel()));
		model.setReturnType(new ClassUsageModel());
		model.setSignature("example()");
		
		model.getReturnType().setClazz(new ClassModel());
		model.getReturnType().getClazz().setName("Test");
		
		MethodData data = converter.convert(model);
		
		Assert.assertNotNull(data);
		Assert.assertEquals("example", data.getName());
		Assert.assertEquals("Lorem ipsum", data.getDocumentation());
		Assert.assertEquals("Test example()", data.getSignature());
		
		Assert.assertNull(data.getParameterTypeTags());
		Assert.assertNull(data.getReturnTypeTag());
		Assert.assertNull(data.getReferences());
	}
}
