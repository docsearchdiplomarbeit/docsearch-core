package codes.probst.docsearch.packages.facade.converter;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.converter.Converter;

public class PackageConverterTest {
	private Converter<PackageModel, PackageData> converter;
	
	@Before
	public void setup() {
		
		converter = new DefaultPackageConverter();
	}
	
	@Test
	public void testModelConversion() {
		PackageModel model = new PackageModel();
		model.setId(Long.valueOf(0l));
		model.setDocumentation("jUnit Documentation");
		model.setName("com.example");
		model.setVersion(new VersionModel());
		model.setClasses(Arrays.asList(new ClassModel()));
		
		PackageData data = converter.convert(model);
		
		Assert.assertNotNull(data);
		Assert.assertEquals("com.example", data.getName());
		Assert.assertEquals(Long.valueOf(0l), data.getId());
		
		Assert.assertNull(data.getUrl());
		Assert.assertNull(data.getDocumentation());
		Assert.assertNull(data.getClasses());
		Assert.assertNull(data.getVersion());
	}
}
