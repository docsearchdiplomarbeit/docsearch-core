package codes.probst.docsearch.packagez.dao;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.docsearch.packages.dao.JpaPackageDao;
import codes.probst.docsearch.packages.dao.PackageDao;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.model.VersionModel;

public class PackageDaoTest {
	private PackageDao packageDao;
	private EntityManager entityManager;
	
	@Before
	public void setup() {
		entityManager = Persistence.createEntityManagerFactory("junit").createEntityManager();
		JpaPackageDao dao = new JpaPackageDao();
		dao.setEntityManager(entityManager);
		packageDao = dao;
	}
	
	@Test
	public void testByNameAndVersion() {
		entityManager.getTransaction().begin();
		
		ProjectModel project = new ProjectModel();
		project.setGroupId("com.example");
		project.setArtifactId("junit");
		
		VersionModel version = new VersionModel();
		version.setCode("0.0.0");
		version.setProject(project);
		
		PackageModel packagez1 = new PackageModel();
		packagez1.setName("com.example");
		packagez1.setVersion(version);
		
		PackageModel packagez2 = new PackageModel();
		packagez2.setName("com.example.sample");
		packagez2.setVersion(version);
		
		entityManager.persist(project);
		entityManager.persist(version);
		entityManager.persist(packagez1);
		entityManager.persist(packagez2);
		
		
		Optional<PackageModel> optionalPackage = packageDao.findByNameAndVersionId("com.example", version.getId());
		
		Assert.assertTrue(optionalPackage.isPresent());
		PackageModel packagez = optionalPackage.get();
		
		Assert.assertEquals("com.example", packagez.getName());
		
		entityManager.getTransaction().rollback();
	}
}
