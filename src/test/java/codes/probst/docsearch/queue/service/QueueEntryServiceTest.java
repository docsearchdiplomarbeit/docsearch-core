package codes.probst.docsearch.queue.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import codes.probst.docsearch.analyzer.projectinformation.ProjectInformation;
import codes.probst.docsearch.analyzer.projectinformation.ProjectInformationAnalyzer;
import codes.probst.docsearch.analyzer.projectinformation.ProjectType;
import codes.probst.docsearch.queue.dao.QueueEntryDao;
import codes.probst.docsearch.queue.model.QueueEntryModel;
import codes.probst.docsearch.test.util.IOUtil;

public class QueueEntryServiceTest {
	private QueueEntryService service;
	private Map<Long, QueueEntryModel> database;
	
	
	@Before
	public void setup() {
		database = new HashMap<>();
		QueueEntryDao dao = Mockito.mock(QueueEntryDao.class);
		Mockito.doAnswer(new Answer<Void>() {
			
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				QueueEntryModel entry = invocation.getArgumentAt(0, QueueEntryModel.class);
				
				Long id = Long.valueOf(database.size());
				entry.setId(id);
				database.put(id, entry);
				
				return null;
			}
			
		}).when(dao).insert(Mockito.any(QueueEntryModel.class));
		
		
		
		DefaultQueueEntryService service = new DefaultQueueEntryService();
		service.setDao(dao);
		service.setProjectInformationAnalyzer(new ProjectInformationAnalyzer() {
			
			@Override
			public Optional<ProjectInformation> analyze(byte[] data) {
				return Optional.of(new ProjectInformation(ProjectType.MAVEN, "junit", "junit", "none"));
			}
		});
		
		this.service = service;
	}
	
	@Test
	public void testImport() throws IOException {
		byte[] compiled = IOUtil.readAll(getClass().getResourceAsStream("example1-0.0.1-SNAPSHOT.jar"), 1024);
		byte[] source = IOUtil.readAll(getClass().getResourceAsStream("example1-0.0.1-SNAPSHOT-sources.jar"), 1024);
		
		service.importEntry(compiled, source);
		
		Assert.assertEquals(1, database.size());
		
		QueueEntryModel entry = database.values().iterator().next();
		
		Assert.assertEquals(Long.valueOf(0), entry.getId());
		Assert.assertEquals("junit", entry.getGroupId());
		Assert.assertEquals("junit", entry.getArtifactId());
		Assert.assertEquals("none", entry.getVersion());
		Assert.assertNull(entry.getPosition());
		Assert.assertEquals(ProjectType.MAVEN, entry.getType());
		Assert.assertEquals(compiled, entry.getCompiledData());
		Assert.assertEquals(source, entry.getSourceData());
	}
	
	@Test
	public void testImportWithPosition() throws IOException {
		byte[] compiled = IOUtil.readAll(getClass().getResourceAsStream("example1-0.0.1-SNAPSHOT.jar"), 1024);
		byte[] source = IOUtil.readAll(getClass().getResourceAsStream("example1-0.0.1-SNAPSHOT-sources.jar"), 1024);
		
		service.importEntry(compiled, source, Long.valueOf(12));
		
		Assert.assertEquals(1, database.size());
		
		QueueEntryModel entry = database.values().iterator().next();
		
		Assert.assertEquals(Long.valueOf(0), entry.getId());
		Assert.assertEquals("junit", entry.getGroupId());
		Assert.assertEquals("junit", entry.getArtifactId());
		Assert.assertEquals("none", entry.getVersion());
		Assert.assertEquals(Long.valueOf(12), entry.getPosition());
		Assert.assertEquals(ProjectType.MAVEN, entry.getType());
		Assert.assertEquals(compiled, entry.getCompiledData());
		Assert.assertEquals(source, entry.getSourceData());
	}
}
