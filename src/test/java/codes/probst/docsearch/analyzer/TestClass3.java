package codes.probst.docsearch.analyzer;

public class TestClass3 {

	private void privateMethod() {
	}
	
	void packagePrivateMethod() {
		
	}
	
	protected void protectedMethod() {
	}
	
	public void publicMethod() {
	}
	
}
