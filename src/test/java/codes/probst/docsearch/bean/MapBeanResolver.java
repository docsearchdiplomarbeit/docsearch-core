package codes.probst.docsearch.bean;

import java.util.HashMap;
import java.util.Map;

import codes.probst.framework.bean.BeanResolver;

public class MapBeanResolver implements BeanResolver {
	private Map<Class<?>, Object> mappings = new HashMap<>();
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T resolve(Class<T> clazz) {
		return (T) mappings.get(clazz);
	}

	public <T> void register(Class<T> clazz, T object) {
		mappings.put(clazz, object);
	}
}
