package codes.probst.docsearch.clazz.facade.converter;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.framework.converter.Converter;

public class ClassConverterTest {
	private Converter<ClassModel, ClassData> converter;
	
	@Before
	public void setup() {
		converter = new DefaultClassConverter();
	}
	
	@Test
	public void testConversion() {
		ClassModel model = new ClassModel();
		model.setName("Example");
		model.setId(Long.valueOf(0l));
		model.setCompilation(new byte[5]);
		model.setSource(new byte[10]);
		model.setDocumentation("Lorem ipsum");
		model.setMethods(Arrays.asList(new MethodModel()));
		model.setPackageModel(new PackageModel());
		
		ClassData data = converter.convert(model);
		
		Assert.assertNotNull(data);
		Assert.assertEquals("Example", data.getName());
		Assert.assertEquals(Long.valueOf(0l), data.getId());
		
		Assert.assertNull(data.getUrl());
		Assert.assertNull(data.getDescription());
		Assert.assertNull(data.getSource());
		Assert.assertNull(data.getMethods());
		Assert.assertNull(data.getPackagez());
		Assert.assertNull(data.getProject());
		Assert.assertNull(data.getVersion());
	}
}
