package codes.probst.docsearch.clazz.url;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.packages.url.DefaultPackageUrlResolver;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.url.NoopUrlCleaner;
import codes.probst.framework.url.UrlResolver;

public class ClassUrlResolverTest {
	private UrlResolver<ClassModel> urlResolver;
	
	@Before
	public void setup() {
		DefaultClassUrlResolver urlResolver = new DefaultClassUrlResolver();
		urlResolver.setUrlCleaner(new NoopUrlCleaner());
		
		this.urlResolver = urlResolver;
	}
	
	@Test
	public void testVersionWithPackageAndVersionAndProject() {
		ClassModel clazz = new ClassModel();
		clazz.setId(Long.valueOf(100l));
		clazz.setName("Example");
		
		PackageModel packagez = new PackageModel();
		packagez.setId(Long.valueOf(10l));
		packagez.setName("com.example");
		
		VersionModel version = new VersionModel();
		version.setCode("1.1");
		version.setId(Long.valueOf(0l));
		
		ProjectModel project = new ProjectModel();
		project.setId(Long.valueOf(5l));
		project.setArtifactId("junit");
		project.setGroupId("junit-group");
		
		version.setProject(project);
		packagez.setVersion(version);
		clazz.setPackageModel(packagez);
		
		String url = urlResolver.resolve(clazz);
		Assert.assertEquals("/projects/junit/5/1.1/0/com.example/10/Example/100", url);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testVersionWithPackageAndVersionWithoutProject() {
		ClassModel clazz = new ClassModel();
		clazz.setId(Long.valueOf(100l));
		clazz.setName("Example");
		
		PackageModel packagez = new PackageModel();
		packagez.setId(Long.valueOf(10l));
		packagez.setName("com.example");
		
		VersionModel version = new VersionModel();
		version.setCode("1.1");
		version.setId(Long.valueOf(0l));
		
		packagez.setVersion(version);
		clazz.setPackageModel(packagez);
		
		urlResolver.resolve(clazz);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testVersionWithPackageWithoutVersionAndProject() {
		ClassModel clazz = new ClassModel();
		clazz.setId(Long.valueOf(100l));
		clazz.setName("Example");
		
		PackageModel packagez = new PackageModel();
		packagez.setId(Long.valueOf(10l));
		packagez.setName("com.example");
		
		clazz.setPackageModel(packagez);
		
		urlResolver.resolve(clazz);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testVersionWithoutPackageAndVersionAndProject() {
		ClassModel clazz = new ClassModel();
		clazz.setId(Long.valueOf(100l));
		clazz.setName("Example");
		
		PackageModel packagez = new PackageModel();
		packagez.setId(Long.valueOf(10l));
		packagez.setName("com.example");
		
		clazz.setPackageModel(packagez);
		
		urlResolver.resolve(clazz);
	}
}
