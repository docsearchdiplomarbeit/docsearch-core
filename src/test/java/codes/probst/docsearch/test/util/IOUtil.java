package codes.probst.docsearch.test.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class IOUtil {
	private IOUtil() {
	}
	
	
	public static byte[] readAll(InputStream is, int bufferSize) throws IOException {
		int bytesRead = 0;
		byte[] buffer = new byte[bufferSize];
		try(ByteArrayOutputStream os = new ByteArrayOutputStream()) {
			while((bytesRead = is.read(buffer)) != -1 ) {
				os.write(buffer, 0, bytesRead);
			}
			
			return os.toByteArray();
		}
	}
}
