package codes.probst.docsearch.project.facade.populator;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import codes.probst.docsearch.bean.MapBeanResolver;
import codes.probst.docsearch.project.facade.ProjectData;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.facade.VersionData;
import codes.probst.docsearch.version.facade.VersionFacade;
import codes.probst.framework.populator.Populator;

public class ProjectVersionsPopulatorTest {
	private Populator<ProjectModel, ProjectData> populator;
	
	@Before
	public void setup() {
		VersionData version1 = new VersionData();
		version1.setCode("1.1");
		version1.setUrl("/version/1.1");

		VersionData version2 = new VersionData();
		version2.setCode("1.2");
		version2.setUrl("/version/1.2");
		
		VersionFacade facade = Mockito.mock(VersionFacade.class);
		Mockito.doReturn(Arrays.asList(version1, version2)).when(facade).getByProjectId(Mockito.anyLong(), Mockito.any());
		
		ProjectVersionsPopulator populator = new ProjectVersionsPopulator();
		MapBeanResolver beanResolver = new MapBeanResolver();
		beanResolver.register(VersionFacade.class, facade);
		populator.setBeanResolver(beanResolver);
		
		this.populator = populator;
	}
	
	@Test
	public void testProjectVersionsPopulator() {
		ProjectData data = new ProjectData();
		populator.populate(new ProjectModel(), data);
		
		Assert.assertNotNull(data.getVersions());
		Assert.assertEquals(2, data.getVersions().size());

		Assert.assertNull(data.getUrl());
		Assert.assertNull(data.getGroupId());
		Assert.assertNull(data.getArtifactId());
		Assert.assertNull(data.getId());
	}
	
}
