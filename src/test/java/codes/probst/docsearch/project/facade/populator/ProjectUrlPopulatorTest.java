package codes.probst.docsearch.project.facade.populator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.docsearch.project.facade.ProjectData;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.url.NoopUrlResolver;
import codes.probst.framework.populator.Populator;

public class ProjectUrlPopulatorTest {
	private Populator<ProjectModel, ProjectData> populator;
	
	@Before
	public void setup() {
		ProjectUrlPopulator populator = new ProjectUrlPopulator();
		populator.setResolver(new NoopUrlResolver<>());
		
		this.populator = populator;
	}
	
	@Test
	public void test() {
		ProjectData data = new ProjectData();
		populator.populate(new ProjectModel(), data);
		
		Assert.assertEquals("/junit/url", data.getUrl());
		
		Assert.assertNull(data.getGroupId());
		Assert.assertNull(data.getArtifactId());
		Assert.assertNull(data.getId());
		Assert.assertNull(data.getVersions());
	}
}
