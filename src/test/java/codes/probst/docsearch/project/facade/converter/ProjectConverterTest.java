package codes.probst.docsearch.project.facade.converter;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.docsearch.project.facade.ProjectData;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.converter.Converter;

public class ProjectConverterTest {
	private Converter<ProjectModel, ProjectData> converter;
	
	@Before
	public void setup() {
		converter = new DefaultProjectConverter();
	}
	
	@Test
	public void testConversion() {
		ProjectModel model = new ProjectModel();
		model.setGroupId("junit-group");
		model.setArtifactId("junit");
		model.setId(Long.valueOf(0l));
		model.setVersions(Arrays.asList(new VersionModel()));
		
		ProjectData data = converter.convert(model);
		
		Assert.assertNotNull(data);
		Assert.assertEquals("junit-group", data.getGroupId());
		Assert.assertEquals("junit", data.getArtifactId());
		Assert.assertEquals(Long.valueOf(0l), data.getId());
		
		Assert.assertNull(data.getUrl());
		Assert.assertNull(data.getVersions());
	}
}
